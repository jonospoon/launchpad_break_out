/*
 * my_sd.c
 *
 *  Created on: Apr 13, 2017
 *      Author: Jonathan
 */
#include "my_sd.h"
#include "fatfs/src/diskio.h"
#include "fatfs/src/ff.h"
#include "my_uart.h"

static char SDLog [10];
static uint8_t RTC_time [5];
static uint16_t year;
char buffer [20];
void write_string_toSD(char * string)
{
	/* SD CARD*/
	FATFS fs;            // Work area (file system object) for logical drive
	FRESULT res;         // FatFs function common result code
	//WORD br, bw;         // File R/W count
	FIL  fdst;      // file objects
	//DSTATUS x = disk_initialize(0);

	//res = f_mount(&fs,1,1);


	res = f_open(&fdst, SDLog, FA_OPEN_APPEND | FA_WRITE);
	if (res != FR_OK) {
		//send_UART("f_mount error: \n");
		//return (1);
	}
	else
	{
		//send_UART("File opened");
	}
	f_printf (&fdst, string);

	f_close(&fdst);
	/* SD CARD*/
}

void createNewSDLog()
{
	sprintf(SDLog,"log%d.dat",ReadSDLogs());
	incrementSDLogFiles();
}



void writeDatetoSD()
{

	getTimefromRTCtoDecArray(RTC_time,&year);
	sprintf(buffer,"Date %d:%d %d\n",RTC_time[3],RTC_time[4],year);
	write_string_toSD(buffer);
}

void writeTimetoSD(){
	getTimefromRTCtoDecArray(RTC_time,&year);
	sprintf(buffer,"Time %d:%d:%d\n",RTC_time[2],RTC_time[1],RTC_time[0]);
	write_string_toSD(buffer);
}

FRESULT mount_sdCard(void)
{
	FATFS fs;
	FRESULT res;
	res = f_mount(&fs,1,1);
	if (res != FR_OK) {
		send_UART("f_mount error: \n");
		//return (1);
	}
	else
	{
		send_UART("SD Card Setup Complete\n");
	}
	createNewSDLog();
	writeDatetoSD();
	writeTimetoSD();

	return res;
}

