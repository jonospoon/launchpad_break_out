/*
 * my_Message.c
 *
 *  Created on: Aug 7, 2017
 *      Author: Jonathan
 */

//#include "my_Message.h"
#include "my_NodeSettings.h"

uint8_t data[] = "Hello World from Red!";
//uint8_t data[] = "0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()";

uint8_t buf[RH_MESH_MAX_MESSAGE_LEN];
uint8_t len = sizeof(buf);
uint8_t from;
uint8_t dest; uint8_t id; uint8_t flags;
uint16_t MessagesPassedOn;
uint16_t MessageReceived=0;
bool Mesh_Receive_internal(void)
{
	uint8_t MeshCheck = MeshrecvfromAckTimeout(buf, &len, 3000, &from,&dest, &id, &flags );
	if (MeshCheck == MeshRecvForMe)
	{
		switch (flags){
		case FLAGS_FROM_RHINO_TROUGH_MESH:
			send_UART("From Rhino\n");
			break;
		case FLAGS_FROM_RHINO_DIRECT:
			send_UART("Direct From Rhino\n");
			break;
		case Node_to_Node:
			send_UART("From a Node\n");
			break;
		}
		MessageReceived++;
		send_UART("Message From : 0x"); /*** TEST ***/
		send_uart_integer(from);		/*** TEST ***/
		send_UART("\n");				/*** TEST ***/
		send_UART((char*)buf);			/*** TEST ***/
		send_UART("\n");				/*** TEST ***/
		send_UART("MessagesReceived: ");
		send_uart_integer(MessageReceived);
		send_UART("\n");
		/*
		if(MeshsendtoWait(data,sizeof(data),from, 0)==RH_ROUTER_ERROR_NONE)
		{
			send_UART("Replied \n");
		}
		 */
		return true;
	}
	else if (MeshCheck == MeshRecvPassedOn)
	{
		/*send_UART("MessagePassedon");
		MessagesPassedOn++;
		send_uart_integer(MessagesPassedOn);		/*** TEST ***/
		//send_UART("\n");
		incrememntMemoryPassedOn();
		//deleteRouteTo(4);
		return true;
	}
	return false;
}
