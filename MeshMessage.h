/*
 * MeshMessage.h
 *
 *  Created on: Jun 12, 2017
 *      Author: Jonathan
 */

#ifndef MESHMESSAGE_H_
#define MESHMESSAGE_H_
#include "driverlib.h"
#include "MessageRouter.h"

// Types of RHMesh message, used to set msgType in the RHMeshHeader
#define RH_MESH_MESSAGE_TYPE_APPLICATION                    	0
#define RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST        	1
#define RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE       	2
#define RH_MESH_MESSAGE_TYPE_ROUTE_FAILURE                  	3
#define RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST_server 	4
#define RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server 	5
#define RH_MESH_MESSAGE_TYPE_BATTERY_LOW						6
#define RH_MESH_MESSAGE_TYPE_BATTERY_HIGH						7


#define MeshRecvError											0
#define MeshRecvForMe											1
#define MeshRecvPassedOn										2


//The maximum number of times a certain route is allowed to fail before it is deleted and another route
//will be need to be found
#define RH_MESH_MAX_ROUTE_RETRIES								3

// Timeout for address resolution in milliecs
#define RH_MESH_ARP_TIMEOUT 20000
#define RH_MESH_MAX_ROUTE__DISCOVERY_MESSAGE_SIZE (RH_MESH_MAX_MESSAGE_LEN - RH_ROUTING_TABLE_SIZE)

/// The maximum length permitted for the application payload data in a RHMesh message
#define RH_MESH_MAX_MESSAGE_LEN (RH_ROUTER_MAX_MESSAGE_LEN - sizeof(MeshMessageHeader))

/// Structure of the basic RHMesh header.
typedef struct
{
	uint8_t             msgType;  ///< Type of RHMesh message, one of RH_MESH_MESSAGE_TYPE_*
} MeshMessageHeader;

/// Signals an application layer message for the caller of RHMesh
typedef struct
{
	MeshMessageHeader   header; ///< msgType = RH_MESH_MESSAGE_TYPE_APPLICATION
	uint8_t             data[RH_MESH_MAX_MESSAGE_LEN]; ///< Application layer payload data
} MeshApplicationMessage;

/// Signals a route discovery request or reply (At present only supports physical dest addresses of length 1 octet)
typedef struct
{
	MeshMessageHeader   header;  ///< msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_*
	uint8_t             destlen; ///< Reserved. Must be 1.g
	uint8_t             dest;    ///< The address of the destination node whose route is being sought
	//bool				server[RH_ROUTING_TABLE_SIZE]; ///< List of whether each node is a server or not. Set by each node individually
	uint8_t             route[RH_MESH_MAX_ROUTE__DISCOVERY_MESSAGE_SIZE -1]; ///< List of node addresses visited so far. Length is implcit
} MeshRouteDiscoveryMessage;

/// Signals a route failure
typedef struct
{
	MeshMessageHeader   header; ///< msgType = RH_MESH_MESSAGE_TYPE_ROUTE_FAILURE
	uint8_t             dest; ///< The address of the destination towards which the route failed
} MeshRouteFailureMessage;
///Signals a node battery failure or node back online status. Will affect routes with the node as a
///destination or having it as a next hop
typedef struct
{
	MeshMessageHeader   header; ///< msgType = RH_MESH_MESSAGE_TYPE_BATTERY_*
	//uint8_t             dest; ///< The address of the node which
}MeshRouteBatteryMessage;

uint8_t MeshsendtoWait(uint8_t* buf, uint8_t len, uint8_t address, uint8_t flags);
uint8_t MeshFromTagtoServer(uint8_t* buf, uint8_t len, uint8_t flags);
bool doArp(uint8_t address, bool serverSearch);

uint8_t Meshroute(RoutedMessage* message, uint8_t messageLen);
void peekAtMessage(RoutedMessage* message, uint8_t messageLen);

uint8_t MeshrecvfromAck(uint8_t* buf, uint8_t* len, uint8_t* source, uint8_t* dest, uint8_t* id, uint8_t* flags, uint8_t from);
uint8_t MeshrecvfromAckTimeout(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags);

bool MeshisPhysicalAddress(uint8_t* address, uint8_t addresslen);
/*
 * Used to alert the neighbours around this node that I am out of battery and cannot
 * be communicated through any longer. The route through this node will not be deleted but simply
 * disabled
 */
uint8_t MeshSendBatteryLow(void);
/*
 * Used to alert the neighbours around this node that the Battery voltage
 * is high enough for communication to occure through this node again
 */
uint8_t MeshSendBatteryHigh(void);

uint8_t checkForServer();


#endif /* MESHMESSAGE_H_ */
