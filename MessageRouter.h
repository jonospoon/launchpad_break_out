/*
 * MessageRouter.h
 *
 *  Created on: May 28, 2017
 *      Author: Jonathan
 */

#ifndef MESSAGEROUTER_H_
#define MESSAGEROUTER_H_

#include"ReliableMessage.h"
// Default max number of hops we will route
#define RH_DEFAULT_MAX_HOPS 30

// The default size of the routing table we keep
#define RH_ROUTING_TABLE_SIZE 10

// Error codes
#define RH_ROUTER_ERROR_NONE              0
#define RH_ROUTER_ERROR_INVALID_LENGTH    1
#define RH_ROUTER_ERROR_NO_ROUTE          2
#define RH_ROUTER_ERROR_TIMEOUT           3
#define RH_ROUTER_ERROR_NO_REPLY          4
#define RH_ROUTER_ERROR_UNABLE_TO_DELIVER 5

// This size of RH_ROUTER_MAX_MESSAGE_LEN is OK for Arduino Mega, but too big for
// Duemilanova. Size of 50 works with the sample router programs on Duemilanova.
#define RH_ROUTER_MAX_MESSAGE_LEN (251 - sizeof(RoutedMessageHeader))

#define FLAGS_FROM_RHINO_TROUGH_MESH 0x48
#define FLAGS_FROM_RHINO_DIRECT 0x28

#define RouterRecvError 					0
#define RouterRecvMessageForMe 				1
#define RouterRecvMessagePassedOn			2



/// Defines the structure of the RHRouter message header, used to keep track of end-to-end delivery parameters
typedef struct
{
	uint8_t    dest;       ///< Destination node address
	uint8_t    source;     ///< Originator node address
	uint8_t    hops;       ///< Hops traversed so far
	uint8_t    id;         ///< Originator sequence number
	uint8_t    flags;      ///< Originator flags
	// Data follows, Length is implicit in the overall message length
} RoutedMessageHeader;

/// Defines the structure of a RHRouter message
typedef struct
{
	RoutedMessageHeader header;    ///< end-to-end delivery header
	uint8_t             data[RH_ROUTER_MAX_MESSAGE_LEN]; ///< Application payload data
} RoutedMessage;

/// Values for the possible states for routes
/*
typedef enum
{
	Invalid = 0,           ///< No valid route is known
	Discovering,           ///< Discovering a route (not currently used)
	Valid                  ///< Route is valid
} RouteState;
*/
#define Invalid false		///< No valid route is known
#define Valid true			///< Route is valid

#define SERVER 		true	///< Node is a server
#define NOT_SERVER 	false 	///< Node is not a server

/// Defines an entry in the routing table
typedef struct
{
	uint8_t      dest;      ///< Destination node address
	uint8_t      next_hop;  ///< Send via this next hop address
	bool      	 state;     ///< State of this route, either valid or invalid
	uint8_t		 hops;		///< Number of hops between this nod and the final destination
	bool 		 server;	///< Whether a node is a server or not
	uint8_t		 RouteFailures;///< Number of failed communication tries to this node
} RoutingTableEntry;

bool setupRouter(uint8_t thisAddress);

void setMaxHops(uint8_t max_hops);

void addRouteTo(uint8_t dest, uint8_t next_hop, bool state, uint8_t hops, bool server);

void sortRoutingTable(void);

RoutingTableEntry* getRouteTo(uint8_t dest);

RoutingTableEntry* getRouteToServer(void);

/*
 * Checks through routing table and any path that has a destination = dest
 * or next_hop = dest will have its state set to invalid but not deleted
 */
bool disableRouteIncluding(uint8_t dest);
/*
 * Checks through routing table and any path that has a destination = dest
 * or next_hop = dest will have its state set to Valid
 */
bool enableRouteIncluding(uint8_t dest);

bool deleteRouteTo(uint8_t dest);

void retireOldestRoute();

void deleteRoute(uint8_t index);

void clearRoutingTable();

uint8_t UpdateRoutingTableInMemory(void);

uint8_t UpdateRoutingTableFromMemory(void);

uint8_t sendtoFromSourceWait(uint8_t* buf, uint8_t len, uint8_t dest, uint8_t source, uint8_t flags);

uint8_t route(RoutedMessage* message, uint8_t messageLen);

uint8_t RouterrecvfromAck(uint8_t* buf, uint8_t* len, uint8_t* source, uint8_t* dest, uint8_t* id, uint8_t* flags, uint8_t from);

bool recvfromAckTimeoutRoute(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* source, uint8_t* dest, uint8_t* id, uint8_t* flags);

uint8_t maxHops(void); //Returns the max hops a msg may take before been dropped through the mesh

/*
 * Returns the route in the table at the position specified. If no route exists then
 * a NULL is returned.
 */
RoutingTableEntry* getRouteBasedOnNumber(uint8_t position);

/*
 * Adds one to the route failure number for a certain route
 */
void addRouteFailure(uint8_t dest);

/*
 * Subtractes one to the route failure number for a certain route
 */
void subtractRouteFailure(uint8_t dest);

/*
 * Returns the number of route failures for a certain route
 */
uint8_t getRouteFailure(uint8_t dest);

void printRoutingTable();

void printServer(RoutingTableEntry* route);

#endif /* MESSAGEROUTER_H_ */
