/*
 * my_RTC.h
 *
 *  Created on: May 16, 2017
 *      Author: Jonathan
 */

#ifndef MY_RTC_H_
#define MY_RTC_H_

#include "driverlib.h"
#include "my_externalFlags.h"
#include "my_gps.h"

 void RTC_setup_begin();
 void RTC_setTime(RTC_C_Calendar currentTime);
 RTC_C_Calendar RTC_C_getCalendarTime_mine(void);
 RTC_C_Calendar RTC_getTime();
 void RTC_begin();
 uint8_t dec2BCD(uint8_t val);
 uint8_t BCD2Dec(uint8_t val);
 void setTimefromDecArray(uint8_t* timeArr, uint16_t year);
 void setTimefromDecCalander(RTC_C_Calendar *time);
 void getTimefromRTCtoDecArray(uint8_t* timeArr, uint16_t *year);
 void getTimefromRTCtoDecCalendar(RTC_C_Calendar *time);
 /*
  * Returns the 5 minute interrupt
  */
 bool minute_5_check(void);
 /*
  * returns the minute interrupt
  */
 bool minute_check(void);
/*
 * Used to set the 5 minute flag
 */
 void set_minute_5_interrupt(bool set);
 /*
  * Used to set the minute interrupt flag
  */
 void set_minute_interrupt(bool set);
 /*
  * Used to set the Real time clock from a value
  * obtained from the GPS
  */
 void set_RTCfromGPS(void);

#endif /* MY_RTC_H_ */
