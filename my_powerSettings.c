/*
 * my_powerSettings.c
 *
 *  Created on: Aug 7, 2017
 *      Author: Jonathan
 */

#include "my_NodeSettings.h"

bool Batteryok;
bool restarted;

bool sleep_all(void)
{
	shutdown_GPS();
	RH_RF95sleep();
	return true;
}

bool shutDown_all(void)
{
	disable_ctrl_pin();
	shutdown_RFM96W();
	return true;
}

void ShutDown_MSP(void)
{
	MeshSendBatteryLow();
	/* Switch all clocks to low-frequency operation prior to LF operations */
	CS_initClockSignal(CS_MCLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	CS_initClockSignal(CS_SMCLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	PCM_setPowerState(PCM_LPM0_LF_VCORE0);
	//#error "Invoke PCM API to change to LPM0 Mode, VCORE = 0 using Low-Frequency Mode"
}
void StartUp_MSP(void)
{
	/* Switch back to using LDO regulator first before increasing the clocks */
	PCM_setPowerState(PCM_AM_LDO_VCORE0);
	//#error "Invoke PCM API to change to Active Mode, VCORE = 0 using LDO"
	/* Switch clocks back to high-frequency operation */
	CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	PCM_setPowerState(PCM_LPM3);
	PCM_setPowerState(PCM_AM_DCDC_VCORE0);

	//Set up all the registers from scratch again
	SystemInit();
	//setup_startup();
	MeshSendBatteryHigh();
	//main();
}
void PORT3_IRQHandler(void)
{
	//Port3 has had an interrupt
	uint32_t status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P3);
	GPIO_clearInterruptFlag(GPIO_PORT_P3, status);

	//Check if it is the pin connected to Vbat_ok
	if (status & GPIO_PIN7)
	{
		//if the transition was from high to low
		//I.E the battery is draining, need to switch off everything and go to sleep
		if(get_batteryOK())
		{
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN0);
			//Set the batterOK to false, and turn off all peripherals
			set_batteryOK(false);
			shutDown_all();
			//Set the VBat_ok interrupt to fire when the battery is high enough to switch everything
			//back on again
			GPIO_interruptEdgeSelect(GPIO_PORT_P3, GPIO_PIN7, GPIO_LOW_TO_HIGH_TRANSITION);
			//Switch the MSP to very slow clock
			ShutDown_MSP();

		}
		//Transition from low to high
		else
		{

			//Set the Vbat_ok interrupt to fire when the battery is low
			GPIO_interruptEdgeSelect(GPIO_PORT_P3, GPIO_PIN7, GPIO_HIGH_TO_LOW_TRANSITION);
			//Set the batterOK to true, and turn on all peripherals
			set_batteryOK(true);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0);
			//Start up the MSP again by restarting the whole MSP from scratch
			MAP_ResetCtl_initiateHardReset();

		}
	}

}

void set_restarted(bool setter)
{
	restarted = setter;
}

bool get_restarted(void)
{
	return restarted;
}

void sleepMSP(void)
{
	MAP_PCM_gotoLPM0();
}

void set_batteryOK(bool setter)
{
	Batteryok = setter;
}
bool get_batteryOK(void)
{
	return Batteryok;
}

void disable_ctrl_pin(void)
{
	MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN5);
}
void enable_ctrl_pin(void)
{
	MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P3, GPIO_PIN5);
}

void TerminateGPIO(void)
{

    P1DIR = 0x00;
    P2DIR = 0x00;
    P3DIR = 0x00;
    P4DIR = 0x00;
    P5DIR = 0x00;
    P6DIR = 0x00;
    P7DIR = 0x00;
    P8DIR = 0x00;
    P9DIR = 0x00;
    P10DIR = 0x00;
    P1REN = 0xff;
    P2REN = 0xff;
    P3REN = 0xff;
    P4REN = 0xff;
    P5REN = 0xff;
    P6REN = 0xff;
    P7REN = 0xff;
    P8REN = 0xff;
    P9REN = 0xff;
    P10REN = 0xff;
    P1OUT = 0x00;
    P2OUT = 0x00;
    P3OUT = 0x00;
    P4OUT = 0x00;
    P5OUT = 0x00;
    P6OUT = 0x00;
    P7OUT = 0x00;
    P8OUT = 0x00;
    P9OUT = 0x00;
    P10OUT = 0x00;

    PSS_setHighSidePerformanceMode(PSS_NORMAL_PERFORMANCE_MODE);
    //PSS_setLowSidePerformanceMode(PSS_NORMAL_PERFORMANCE_MODE);
    //PCM_enableRudeMode();
}
