/*
 * my_Flash.h
 *
 *  Created on: Aug 2, 2018
 *      Author: Jonathan
 */

#ifndef MY_FLASH_H_
#define MY_FLASH_H_

#include "my_NodeSettings.h"

#define CALIBRATION_START 0x0003F000

#define ERROR_Erase 1;
#define ERROR_Write 2;
#define Completed 0;
#define NoUpdateNeeded 3;

#define MemAddress 0
#define MemServer 1
#define MemRestarts 2
#define MemUpdates 6
#define MemSDLogfiles 10
#define Mem37Down 14
#define Mem35Down 18
#define MemReceived 22
#define ReceviedfromRhino 26
#define MemPassedOn 30
#define MemReAck 34
#define MemReRoute 38
#define MemReRouteReceived 42
#define MemSent 46
#define MemRetries 50
#define MemFails 54
#define MemAttention 58
#define MemTimeout 62
#define MemRoutingTable 66

#define MemLength 127

void UpdateRequired();
void UpdatedCompleted();
uint8_t CheckMemoryAndUpdate(void);

uint8_t FlashWriteAddress(uint8_t address);
uint8_t FlashReadAdress();
uint8_t FlashWriteFullBuffer(void);
void ReadAllMemory();
uint8_t EraseEntireSector();
void store32bitsToArray(uint32_t integer, uint8_t address);
uint8_t read32bitsFromMemoryToArray(uint8_t address);
uint32_t read32bitsFromArray(uint8_t address);
uint32_t return32bitsFromMemory(uint8_t address);
void zeroNodeStats();
void zeroNetworkStats();
void printAllStats();
void zeroRoutingTable();

uint8_t returnBooleanToInt(bool ChangeBool);
bool returnIntToBoolean(uint8_t ChangeInt);

//uint8_t ReturnFullRoutingTable(RoutingTableEntry * routes);
//uint8_t updateRoutingTable(RoutingTableEntry * routes);
uint8_t ReadRoutingTableFromMemory();

bool FlashReadServer();
uint32_t ReadRestarts();
uint32_t ReadSent();
uint32_t ReadRetries();
uint32_t ReadFails();
uint32_t ReadAttention();
uint32_t ReadRecieved();
uint32_t ReadTimeout();
uint32_t ReadPassedOn();
uint32_t ReadReAck();
uint32_t ReadReRoute();
uint32_t ReadReRoute_Received();
uint32_t ReadMemoryUpdates();
uint32_t ReadSDLogs();
uint32_t Read37DOwn();
uint32_t ReadMem35Down();
uint32_t ReadRecievedFromRhino();

void updateServerStatus(bool Updatedserver);
void updateRestarts(uint32_t number);
void updateSent(uint32_t number);
void updateRetries(uint32_t number);
void updateFails(uint32_t number);
void updateAttention(uint32_t number);
void updateReceived(uint32_t number);
void updateTimeout(uint32_t number);
void updatePassedOn(uint32_t number);
void updateReAck(uint32_t number);
void updateReRoute(uint32_t number);
void updateReRouteReceived(uint32_t number);
void updateMemoryUpdates(uint32_t number);
void updateSDCardLogs(uint32_t number);
void update37Down(uint32_t number);
void updateMem35Down(uint32_t number);
void updateReceivedFromRhino(uint32_t number);


#endif /* MY_FLASH_H_ */
