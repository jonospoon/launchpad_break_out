/*
 * timer.h
 *
 *  Created on: Apr 11, 2017
 *      Author: Jonathan
 */

#ifndef TIMER_H_
#define TIMER_H_

/*
 * interrupt_delay(int ms) will cause the program to delay for a certain time period by using the
 * millisecond clock counter.
 */
void interrupt_delayms(int ms);
/*
 * setup_Timerms_A() is a function which is called to set up the timer for timerA1.
 * All the parameters have already been set to create an interrupt every 1ms when the clock
 * is at 12Mhz
 */
void setup_Timerms_A();




#endif /* TIMER_H_ */
