/*
 * my_Message.h
 *
 *  Created on: Aug 7, 2017
 *      Author: Jonathan
 */

#ifndef MY_MESSAGE_H_
#define MY_MESSAGE_H_

#include "driverlib.h"
//#include "MeshMessage.h"
//#include "my_uart.h"

bool Mesh_Receive_internal(void);

#endif /* MY_MESSAGE_H_ */
