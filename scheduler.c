/*
 * scheduler.c
 *
 *  Created on: Aug 4, 2017
 *      Author: Jonathan
 */

//#include "scheduler.h"
#include "my_NodeSettings.h"
bool sleep_cycle = false;
bool set_receive = false;
uint16_t receive_check_worked=0;





uint8_t RTC_time [5];
int32_t temp;
uint16_t year;

void schedule(void)
{
	if (SystimerReadyCheck()&get_batteryOK())
	{
		if (sleep_cycle)
		{
			//sleep_all();
			//setModeIdle();
			//RH_RF95sleep();
			sleep_cycle = false;
			set_receive = true;
			MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
			//runsystickFunction_ms(2200);
			//setModeIdle();
			return;
		}
		if (set_receive)
		{
			//wakeupRFM();
			//isChannelActive();
			//interrupt_delayms(20);
			//wakeupRFM();
			//clearBuffers();
			MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN0);
			//setModeIdle();
			//wakeupRFM();
			if (Mesh_Receive_internal())
			{
				receive_check_worked++;

			}
			set_receive=false;
			sleep_cycle=true;
			//RH_RF95sleep();
			//RH_RF95sleep();
			//wakeupRFM();
			//setModeIdle();
			//setModeIdle();
			//runsystickFunction_ms(700);
			return;

		}
	}
	if(minute_check())
	{
		getTimefromRTCtoDecArray(RTC_time,&year);
		char buffer[40];
		sprintf(buffer,"The temp is: %d \n"
				"The time is %d:%d:%d %d\n",temp,RTC_time[2],RTC_time[1],RTC_time[0],year);
		send_UART(buffer);
		set_minute_interrupt(false);
		printRoutingTable();
	}
	if(minute_5_check)
	{

	}
}

void set_sleep_cycle(bool setter)
{
	sleep_cycle = setter;
}
void set_set_receive(bool setter)
{
	set_receive = setter;
}
