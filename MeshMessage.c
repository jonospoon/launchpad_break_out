/*
 * MeshMessage.c
 *
 *  Created on: Jun 12, 2017
 *      Author: Jonathan
 */
#include "MeshMessage.h"

static RoutedMessage _tmpMessageMesh;
bool busy_doing_route_find =false;
uint16_t attention = 0;
uint16_t timeouts = 0;
uint16_t reroutes = 0;
uint16_t rerouteRecevied = 0;
////////////////////////////////////////////////////////////////////
// Discovers a route to the destination (if necessary), sends and
// waits for delivery to the next hop (but not for delivery to the final destination)
uint8_t MeshsendtoWait(uint8_t* buf, uint8_t len, uint8_t address, uint8_t flags)
{
	if (len > RH_MESH_MAX_MESSAGE_LEN)
		return RH_ROUTER_ERROR_INVALID_LENGTH;

	if (address != RH_BROADCAST_ADDRESS)
	{
		RoutingTableEntry* route = getRouteTo(address);
		if (!route && !doArp(address, false))
			return RH_ROUTER_ERROR_NO_ROUTE;
	}

	// Now have a route. Contruct an application layer message and send it via that route
	MeshApplicationMessage* a = (MeshApplicationMessage*)&_tmpMessageMesh;
	a->header.msgType = RH_MESH_MESSAGE_TYPE_APPLICATION;
	memcpy(a->data, buf, len);
	return sendtoFromSourceWait((uint8_t*)&_tmpMessageMesh, sizeof(MeshMessageHeader) + len, address, thisAddress(), flags);
}

uint8_t checkForServer()
{
	send_UART("Checking for Nearest Server\n");
	if (AmIServer())
	{
		send_UART("I am server\n");
		return 1;
	}
	RoutingTableEntry* route = NULL;
	route = getRouteToServer();
	if (route)
	{
		send_UART("Server Stored in Routing table\n");
		printServer (route);
		return 1;
	}
	else {
		send_UART("Searching for nearest server");
		if(doArp(0, SERVER))
		{
			send_UART("Server Found");
			route = getRouteToServer();
			printServer (route);
			return 1;
		}
	}
	send_UART("No Server Found");
	return 0;

}



uint8_t MeshSendBatteryLow(void)
{
	uint8_t i = 0;
	for(i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
	{
		RoutingTableEntry* route = getRouteBasedOnNumber(i);
		MeshRouteBatteryMessage* a = (MeshRouteBatteryMessage*)&_tmpMessageMesh;
		//a->dest = thisAddress();
		a->header.msgType = RH_MESH_MESSAGE_TYPE_BATTERY_LOW;
		sendtoFromSourceWait((uint8_t*)&_tmpMessageMesh, sizeof(MeshMessageHeader),route->next_hop, thisAddress(),Node_to_Node);
	}
	return 0;
}

uint8_t MeshSendBatteryHigh(void)
{
	uint8_t i = 0;
	for(i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
	{
		RoutingTableEntry* route = getRouteBasedOnNumber(i);
		MeshRouteBatteryMessage* a = (MeshRouteBatteryMessage*)&_tmpMessageMesh;
		a->header.msgType = RH_MESH_MESSAGE_TYPE_BATTERY_HIGH;
		//a->dest = thisAddress();
		sendtoFromSourceWait((uint8_t*)&_tmpMessageMesh, sizeof(MeshMessageHeader),route->next_hop, thisAddress(),Node_to_Node);
	}
	return 0;
}

uint8_t MeshFromTagtoServer(uint8_t* buf, uint8_t len, uint8_t flags)
{
	//This message is from a Rhino tag
	if (len > RH_MESH_MAX_MESSAGE_LEN)
		return RH_ROUTER_ERROR_INVALID_LENGTH;
	//Search for a server in our routing table to send the data to
	RoutingTableEntry* route = getRouteToServer();
	//If theres no server in our routing table do a arpSearch for
	//a server
	if (!route)
		if(!doArp(0, SERVER))
			return RH_ROUTER_ERROR_NO_ROUTE;
	//We found a route using arp search so get the route from our table now
	route = getRouteToServer();
	// Now have a route. Contruct an application layer message and send it via that route
	MeshApplicationMessage* a = (MeshApplicationMessage*)&_tmpMessageMesh;
	a->header.msgType = RH_MESH_MESSAGE_TYPE_APPLICATION;
	memcpy(a->data, buf, len);
	return sendtoFromSourceWait((uint8_t*)&_tmpMessageMesh, sizeof(MeshMessageHeader) + len, route->dest, thisAddress(), flags);
}

bool doArp(uint8_t address, bool serverSearch)
{
	/*
	reroutes++;
	send_UART("ReRoute: ");
	send_uart_integer(reroutes);
	send_UART("\n");
	*/
	incrememntMemoryReRoute();
	//Set the busy route finding flag high
	busy_doing_route_find = true;
	// Need to discover a route
	// Broadcast a route discovery message with nothing in it
	MeshRouteDiscoveryMessage* p = (MeshRouteDiscoveryMessage*)&_tmpMessageMesh;
	p->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST;
	if (serverSearch== true)
		p->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST_server;
	p->destlen = 1;
	p->dest = address; // Who we are looking for
	uint8_t error = sendtoFromSourceWait((uint8_t*)p, sizeof(MeshMessageHeader) + 2, RH_BROADCAST_ADDRESS,thisAddress(),0);
	if (error !=  RH_ROUTER_ERROR_NONE)
	{
		busy_doing_route_find = false;
		return false;
	}

	// Wait for a reply, which will be unicast back to us
	// It will contain the complete route to the destination
	uint8_t messageLen = sizeof(_tmpMessageMesh);
	// FIXME: timeout should be configurable
	//unsigned long starttime = millis();
	//int32_t timeLeft;
	runsystickFunction_ms(RH_MESH_ARP_TIMEOUT);
	while (!SystimerReadyCheck())
	{
		if (recvfromAckTimeoutRoute((uint8_t*)&_tmpMessageMesh, &messageLen, delayLeft(),0,0,0,0))
		{

			if (   messageLen > 1
					&& ((p->header.msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE)||(p->header.msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server)))
			{
				// Got a reply, now add the next hop to the dest to the routing table
				// The first hop taken is the first octet
				uint8_t numRoutes = messageLen - sizeof(MeshMessageHeader) - 2;
				if (p->header.msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server)
					addRouteTo(address, headerFrom(),Valid,numRoutes, SERVER);
				else
					addRouteTo(address, headerFrom(),Valid,numRoutes, NOT_SERVER);
				busy_doing_route_find = false;
				/*rerouteRecevied++;
				send_UART("ReRoute Received: ");
					send_uart_integer(rerouteRecevied);
					send_UART("\n");
					*/
				incrememntMemoryReRouteRecevied();
				UpdateRoutingTableInMemory();
				return true;
			}
		}
		__no_operation();
	}
	busy_doing_route_find = false;
	return false;
}

// Called by RHRouter::recvfromAck whenever a message goes past
void peekAtMessage(RoutedMessage* message, uint8_t messageLen)
{

	MeshMessageHeader* m = (MeshMessageHeader*)message->data;
	if (   messageLen > 1
			&& ((m->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE)||(m->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server)))
	{
		// This is a unicast RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE messages
		// being routed back to the originator here. Want to scrape some routing data out of the response
		// We can find the routes to all the nodes between here and the responding node
		MeshRouteDiscoveryMessage* d = (MeshRouteDiscoveryMessage*)message->data;
		uint8_t numRoutes = messageLen - sizeof(RoutedMessageHeader) - sizeof(MeshMessageHeader) - 2;


		//Check if the responding msg is from a server or not
		//If it is a simple response then it is known that the message is from a simple node
		//and therefore the node is added as a NOT_SERVER
		//If it is the _server response then the destination node is added as a server
		if (m->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE)
			addRouteTo(d->dest, headerFrom(),Valid,numRoutes,NOT_SERVER);
		else
			addRouteTo(d->dest, headerFrom(),Valid,numRoutes,SERVER);

		uint8_t i;
		// Find us in the list of nodes that were traversed to get to the responding node
		for (i = 0; i < numRoutes; i++)
			if (d->route[i] == thisAddress())
				break;
		i++;
		//All other nodes are added in the route as non servers
		//FIXME: Make sure that non-zero routes are only added
		while (i++ < numRoutes)
			addRouteTo(d->route[i], headerFrom(),Valid,i,NOT_SERVER);
	}
	else if (   messageLen > 1
			&& m->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_FAILURE)
	{
		MeshRouteFailureMessage* d = (MeshRouteFailureMessage*)message->data;
		deleteRouteTo(d->dest);
	}
	/*
	 * Sets the state of the route or next_hop to this node to Invalid,
	 * as its battery is too low
	 */
	else if ( messageLen > 1
			&& m->msgType == RH_MESH_MESSAGE_TYPE_BATTERY_LOW)
	{
		//MeshRouteBatteryMessage* d = (MeshRouteBatteryMessage*)message->data;
		disableRouteIncluding(message->header.source);
		printRoutingTable();
	}
	/*
	 * Restores the state of the route to Valid
	 */
	else if ( messageLen > 1
			&& m->msgType == RH_MESH_MESSAGE_TYPE_BATTERY_HIGH)
	{
		//MeshRouteBatteryMessage* d = (MeshRouteBatteryMessage*)message->data;
		enableRouteIncluding(message->header.source);
		printRoutingTable();
	}

}

// This is called when a message is to be delivered to the next hop
uint8_t Meshroute(RoutedMessage* message, uint8_t messageLen)
{
	uint8_t from = headerFrom(); // Might get clobbered during call to superclass route()
	uint8_t ret = route(message, messageLen);
	if (   ret == RH_ROUTER_ERROR_NO_ROUTE
			|| ret == RH_ROUTER_ERROR_UNABLE_TO_DELIVER)
	{
		//The route failed, so add one to the route failure number
		addRouteFailure(message->header.dest);
		if (getRouteFailure(message->header.dest)>=RH_MESH_MAX_ROUTE_RETRIES)
		{
			// Cant deliver to the next hop. Delete the route
			deleteRouteTo(message->header.dest);
			if (message->header.source != thisAddress())
			{
				// This is being proxied, so tell the originator about it
				MeshRouteFailureMessage* p = (MeshRouteFailureMessage*)&_tmpMessageMesh;
				p->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_FAILURE;
				p->dest = message->header.dest; // Who you were trying to deliver to
				// Make sure there is a route back towards whoever sent the original message
				//The number RH_DEFAULT_MAX_HOPS is used for the number of hops due to the fact that
				//we are not actually sure how far away the node is from us, and so we had the max number of hops
				//to ensure that once it is found out it will definitely be updated.
				addRouteTo(message->header.source, from, Valid,RH_DEFAULT_MAX_HOPS,NOT_SERVER);
				ret = sendtoFromSourceWait((uint8_t*)p, sizeof(MeshMessageHeader) + 1, message->header.source, thisAddress(),0);
			}
		}
	}
	else
	{
		//The route worked so subract one from the route failure number
		subtractRouteFailure(message->header.dest);
	}
	return ret;
}

bool MeshisPhysicalAddress(uint8_t* address, uint8_t addresslen)
{
	// Can only handle physical addresses 1 octet long, which is the physical node address
	return addresslen == 1 && address[0] == thisAddress();
}

uint8_t MeshrecvfromAck(uint8_t* buf, uint8_t* len, uint8_t* source, uint8_t* dest, uint8_t* id, uint8_t* flags, uint8_t from)
{
	uint8_t tmpMessageLen = sizeof(_tmpMessageMesh);
	uint8_t _source;
	uint8_t _dest;
	uint8_t _id;
	uint8_t _flags;
	uint8_t checkRouter = RouterrecvfromAck((uint8_t*)&_tmpMessageMesh, &tmpMessageLen, &_source, &_dest, &_id, &_flags, from);
	if (checkRouter == RouterRecvMessageForMe)
	{
		MeshMessageHeader* p = (MeshMessageHeader*)&_tmpMessageMesh;

		if (   tmpMessageLen >= 1
				&& (p->msgType == RH_MESH_MESSAGE_TYPE_APPLICATION||isFromATag(_flags)))
		{
			MeshApplicationMessage* a = (MeshApplicationMessage*)p;
			// Handle application layer messages, presumably for our caller
			if (source) *source = _source;
			if (dest)   *dest   = _dest;
			if (id)     *id     = _id;
			if (flags)  *flags  = _flags;
			uint8_t msgLen = tmpMessageLen - sizeof(MeshMessageHeader);
			if (*len > msgLen)
				*len = msgLen;
			memcpy(buf, a->data, msgLen);

			return MeshRecvForMe;
		}
		else if (   _dest == RH_BROADCAST_ADDRESS
				&& tmpMessageLen > 1
				&& ((p->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST)||(p->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST_server)))
		{
			MeshRouteDiscoveryMessage* d = (MeshRouteDiscoveryMessage*)p;
			// Handle Route discovery requests
			// Message is an array of node addresses the route request has already passed through
			// If it originally came from us, ignore it
			if (_source == thisAddress())
				return MeshRecvError;

			uint8_t numRoutes = tmpMessageLen - sizeof(MeshMessageHeader) - 2;
			uint8_t i;
			// Are we already mentioned?
			for (i = 0; i < numRoutes; i++)
				if (d->route[i] == thisAddress())
					return MeshRecvError; // Already been through us. Discard

			// Hasnt been past us yet, record routes back to the earlier nodes
			addRouteTo(_source, headerFrom(), Valid, numRoutes,NOT_SERVER); // The originator
			for (i = 0; i < numRoutes; i++)
				addRouteTo(d->route[i], headerFrom(), Valid,i,NOT_SERVER);
			if (MeshisPhysicalAddress(&d->dest, d->destlen))
			{
				// This route discovery is for us. Unicast the whole route back to the originator
				// as a RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE
				// We are certain to have a route there, because we just got it
				d->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE;
				if (AmIServer())
					d->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server;

				sendtoFromSourceWait((uint8_t*)d, tmpMessageLen, _source, thisAddress(),0);
				return MeshRecvPassedOn;
			}
			else if ((p->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST_server)&&AmIServer())
			{
				//This route discovery is for the nearest server. I am a server and therefore I can respond
				//with a RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server
				d->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server;
				d->dest = thisAddress();
				sendtoFromSourceWait((uint8_t*)d, tmpMessageLen, _source, thisAddress(),0);
				return MeshRecvPassedOn;
			}
			else if (i < maxHops())
			{
				//Lets check if we have the route they are looking for in our route list!
				RoutingTableEntry* route = NULL;
				if (p->msgType == RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_REQUEST_server)
					route = getRouteToServer();
				else if  (d->dest!=0)
					route = getRouteTo(d->dest);

				if (route)
				{
					//We have the route in our list. Now its time to Unicast the route back to the originator
					d->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE;
					//Check whether the node is stored as a server and then respond as such
					if (route->server)
						d->header.msgType = RH_MESH_MESSAGE_TYPE_ROUTE_DISCOVERY_RESPONSE_server;
					d->route[numRoutes] = thisAddress();
					tmpMessageLen= tmpMessageLen+1+route->hops;
					sendtoFromSourceWait((uint8_t*)d, tmpMessageLen, _source, thisAddress(),0);
					return MeshRecvPassedOn;
				}
				else
				{
					// Its for someone else, and we dont have it in our routes
					//rebroadcast it, after adding ourselves to the list
					d->route[numRoutes] = thisAddress();
					tmpMessageLen++;
					// Have to impersonate the source
					// REVISIT: if this fails what can we do?
					sendtoFromSourceWait((uint8_t*)&_tmpMessageMesh, tmpMessageLen, RH_BROADCAST_ADDRESS, _source,0);
					return MeshRecvPassedOn;
					//runsystickFunction_ms(4000);
					//while (!SystimerReadyCheck())
					//	{__no_operation();}
				}
			}
		}
	}
	else if (checkRouter == RouterRecvMessagePassedOn)
	{
		return MeshRecvPassedOn;
	}

	return MeshRecvError;
}

uint8_t MeshrecvfromAckTimeout(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
	uint8_t fromCheck;
	if (!recvCheckCanItalk(buf,timeout, &fromCheck))
		return false;
	/*attention++;
	send_UART("Attention ");
	send_uart_integer(attention);
	send_UART("\n");*/
	incrememntMemoryAttention();
	runsystickFunction_ms(timeout);
	runsystickFunction_ms(8000);
	while (!SystimerReadyCheck())
	{
		if (waitAvailableTimeout(delayLeft()))
		{
			uint8_t MeshCheck = MeshrecvfromAck(buf, len, from, to, id, flags, fromCheck);
			if (MeshCheck != MeshRecvError)
				return MeshCheck;
			__no_operation();
		}
	}
	/*
	timeouts++;
	send_UART("Timeout ");
	send_uart_integer(timeouts);
	send_UART("\n");*/
	incrememntMemoryTimeout();
	return MeshRecvError;
}


