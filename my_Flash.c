/*
 * my_Flash.c
 *
 *  Created on: Aug 2, 2018
 *      Author: Jonathan
 */
#include "my_Flash.h"

/* Statics */
static uint8_t simulatedCalibrationData[4096];
static bool UpdateNeeded;


uint8_t FlashStartUp()
{
    ReadAllMemory();
    printAllStats();
    UpdateRoutingTableFromMemory();
    return 1;
}

uint8_t CheckMemoryAndUpdate(void)
{
    if (UpdateNeeded)
    {
        incrememntMemoryUpdates();
        //send_UART("Mem updated");
        return FlashWriteFullBuffer();
    }
    return NoUpdateNeeded;
}

uint8_t FlashWriteFullBuffer(void){

    /* Unprotecting Info Bank 0, Sector 0  */
    MAP_FlashCtl_unprotectSector(FLASH_MAIN_MEMORY_SPACE_BANK1,FLASH_SECTOR31);

    /* Trying to erase the sector. Within this function, the API will
	        automatically try to erase the maximum number of tries. If it fails,
	         trap in an infinite loop */
    if(!MAP_FlashCtl_eraseSector(CALIBRATION_START))
        return ERROR_Erase;

    /* Trying to program the memory. Within this function, the API will
	        automatically try to program the maximum number of tries. If it fails,
	        trap inside an infinite loop */
    if(!MAP_FlashCtl_programMemory(simulatedCalibrationData,
                                   (void*) CALIBRATION_START, 4096))
        return ERROR_Write;

    /* Setting the sector back to protected  */
    MAP_FlashCtl_protectSector(FLASH_MAIN_MEMORY_SPACE_BANK1,FLASH_SECTOR31);
    UpdatedCompleted();
    return Completed;
}

void UpdatedCompleted()
{
    UpdateNeeded = false;

}
void UpdateRequired()
{
    UpdateNeeded = true;
}

uint8_t FlashWriteAddress(uint8_t address)
{
    simulatedCalibrationData[0] = address;
    setRFMAddress(address);
    return FlashWriteFullBuffer();

}

uint8_t FlashReadAdress()
{
    return *(uint8_t*)CALIBRATION_START;
}

bool FlashReadServer()
{
	return returnIntToBoolean(*(uint8_t*)(CALIBRATION_START+MemServer));
}

void updateServerStatus(bool Updatedserver)
{
	simulatedCalibrationData[MemServer] = returnBooleanToInt(Updatedserver);
	//FlashWriteFullBuffer();
	 UpdateRequired();
	 CheckMemoryAndUpdate();
}

uint32_t ReadRestarts()
{
    return return32bitsFromMemory(MemRestarts);
}
void updateRestarts(uint32_t number){
    store32bitsToArray(number,MemRestarts);
    UpdateRequired();
}

uint32_t ReadSent()
{
    return return32bitsFromMemory(MemSent);
}

void updateSent(uint32_t number){
    store32bitsToArray(number,MemSent);
    UpdateRequired();
}

uint32_t ReadRetries()
{
    return read32bitsFromArray(MemRetries);
}
void updateRetries(uint32_t number){
    store32bitsToArray(number,MemRetries);
    UpdateRequired();
}

uint32_t ReadFails()
{
    return return32bitsFromMemory(MemFails);
}
void updateFails(uint32_t number){
    store32bitsToArray(number,MemFails);
    UpdateRequired();
}
uint32_t ReadAttention()
{
    return return32bitsFromMemory(MemAttention);
}
void updateAttention(uint32_t number){
    store32bitsToArray(number,MemAttention);
    UpdateRequired();
}
uint32_t ReadRecieved()
{
    return return32bitsFromMemory(MemReceived);
}
void updateReceived(uint32_t number){
    store32bitsToArray(number,MemReceived);
    UpdateRequired();
}
uint32_t ReadRecievedFromRhino()
{
    return return32bitsFromMemory(ReceviedfromRhino);
}
void updateReceivedFromRhino(uint32_t number){
    store32bitsToArray(number,ReceviedfromRhino);
    UpdateRequired();
}


uint32_t ReadTimeout()
{
    return return32bitsFromMemory(MemTimeout);
}
void updateTimeout(uint32_t number){
    store32bitsToArray(number,MemTimeout);
    UpdateRequired();
}
uint32_t ReadPassedOn()
{
    return return32bitsFromMemory(MemPassedOn);
}
void updatePassedOn(uint32_t number){
    store32bitsToArray(number,MemPassedOn);
    UpdateRequired();
}
uint32_t ReadReAck()
{
    return return32bitsFromMemory(MemReAck);
}
void updateReAck(uint32_t number){
    store32bitsToArray(number,MemReAck);
    UpdateRequired();
}
uint32_t ReadReRoute()
{
    return return32bitsFromMemory(MemReRoute);
}
void updateReRoute(uint32_t number){
    store32bitsToArray(number,MemReRoute);
    UpdateRequired();
}
uint32_t ReadReRoute_Received()
{
    return return32bitsFromMemory(MemReRouteReceived);
}
void updateReRouteReceived(uint32_t number){
    store32bitsToArray(number,MemReRouteReceived);
    UpdateRequired();
}

uint32_t ReadMemoryUpdates()
{
    return return32bitsFromMemory(MemUpdates);
}
void updateMemoryUpdates(uint32_t number)
{
    store32bitsToArray(number,MemUpdates);
    UpdateRequired();
}

uint32_t ReadSDLogs()
{
	return return32bitsFromMemory(MemSDLogfiles);
}

void updateSDCardLogs(uint32_t number)
{
	store32bitsToArray(number,MemSDLogfiles);
	    UpdateRequired();
}
uint32_t Read37DOwn()
{
	return return32bitsFromMemory(Mem37Down);
}

void update37Down(uint32_t number)
{
	store32bitsToArray(number,Mem37Down);
	    UpdateRequired();
}
uint32_t ReadMem35Down()
{
	return return32bitsFromMemory(Mem35Down);
}

void updateMem35Down(uint32_t number)
{
	store32bitsToArray(number,Mem35Down);
	    UpdateRequired();
}

void printAllStats()
{
    send_UART("Node Restarts: ");
    send_uart_integer(read32bitsFromArray(MemRestarts));
    send_UART("\nMemUpdates: ");
    send_uart_integer(read32bitsFromArray(MemUpdates));
    send_UART("\nSD Card LogFiles: ");
    send_uart_integer(read32bitsFromArray(MemSDLogfiles));
    send_UART("\n3.7V ShutDowns: ");
    send_uart_integer(read32bitsFromArray(Mem37Down));
    send_UART("\n3.5V ShutDowns: ");
    send_uart_integer(read32bitsFromArray(Mem35Down));
    send_UART("\nSent: ");
    send_uart_integer(read32bitsFromArray(MemSent));
    send_UART("\nRetries: ");
    send_uart_integer(read32bitsFromArray(MemRetries));
    send_UART("\nFailures: ");
    send_uart_integer(read32bitsFromArray(MemFails));
    send_UART("\nAttention: ");
    send_uart_integer(read32bitsFromArray(MemAttention));
    send_UART("\nReceived: ");
    send_uart_integer(read32bitsFromArray(MemReceived));
    send_UART("\nRecevied From Rhino");
    send_uart_integer(read32bitsFromArray(ReceviedfromRhino));
    send_UART("\nTimeouts: ");
    send_uart_integer(read32bitsFromArray(MemTimeout));
    send_UART("\nPassed On:: ");
    send_uart_integer(read32bitsFromArray(MemPassedOn));
    send_UART("\nRe Ack:: ");
    send_uart_integer(read32bitsFromArray(MemReAck));
    send_UART("\nReRoute: ");
    send_uart_integer(read32bitsFromArray(MemReRoute));
    send_UART("\nReRouteReceived: ");
    send_uart_integer(read32bitsFromArray(MemReRouteReceived));

    send_UART("\n");
}




void zeroNodeStats()
{
    memset(&simulatedCalibrationData[MemRestarts], 0x00, 20);
    FlashWriteFullBuffer();
}

void zeroNetworkStats()
{
    memset(&simulatedCalibrationData[MemReceived], 0x00, 40);
    FlashWriteFullBuffer();
}



void zeroRoutingTable()
{
    memset(&simulatedCalibrationData[MemRoutingTable], 0x00, 60);
    FlashWriteFullBuffer();
}

uint8_t updateRoutingTable(RoutingTableEntry * routes)
{
    uint8_t i;
    for(i=0; i < RH_ROUTING_TABLE_SIZE; i++)
    {
        simulatedCalibrationData[MemRoutingTable+i*5] = routes[i].dest;
        simulatedCalibrationData[MemRoutingTable+i*5+1] = routes[i].next_hop;
        simulatedCalibrationData[MemRoutingTable+i*5+2] = returnBooleanToInt(routes[i].state);
        simulatedCalibrationData[MemRoutingTable+i*5+3] = routes[i].hops;
        simulatedCalibrationData[MemRoutingTable+i*5+4] = returnBooleanToInt(routes[i].server);
        simulatedCalibrationData[MemRoutingTable+i*5+5] =routes[i].RouteFailures;
    }

    return FlashWriteFullBuffer();
}

uint8_t ReadRoutingTableFromMemory()
{
    uint8_t i;
    for(i=0; i < RH_ROUTING_TABLE_SIZE; i++)
    {
        simulatedCalibrationData[MemRoutingTable+i*5]   = *(uint8_t*)(MemRoutingTable+i*5+CALIBRATION_START);
        simulatedCalibrationData[MemRoutingTable+i*5+1] = *(uint8_t*)(MemRoutingTable+i*5+1+CALIBRATION_START);
        simulatedCalibrationData[MemRoutingTable+i*5+2] = *(uint8_t*)(MemRoutingTable+i*5+2+CALIBRATION_START);
        simulatedCalibrationData[MemRoutingTable+i*5+3] = *(uint8_t*)(MemRoutingTable+i*5+3+CALIBRATION_START);
        simulatedCalibrationData[MemRoutingTable+i*5+4] = *(uint8_t*)(MemRoutingTable+i*5+4+CALIBRATION_START);
        simulatedCalibrationData[MemRoutingTable+i*5+5] = *(uint8_t*)(MemRoutingTable+i*5+5+CALIBRATION_START);
    }
    return Completed;
}

uint8_t ReturnFullRoutingTable(RoutingTableEntry * routes)
{
    ReadRoutingTableFromMemory();
    uint8_t i;
    for(i=0; i< RH_ROUTING_TABLE_SIZE; i++)
    {
        routes[i].dest = simulatedCalibrationData[MemRoutingTable+i*5];
        routes[i].next_hop = simulatedCalibrationData[MemRoutingTable+i*5+1];
        routes[i].state = returnIntToBoolean(simulatedCalibrationData[MemRoutingTable+i*5+2]);
        routes[i].hops = simulatedCalibrationData[MemRoutingTable+i*5+3];
        routes[i].server = returnIntToBoolean(simulatedCalibrationData[MemRoutingTable+i*5+4]);
        routes[i].RouteFailures = simulatedCalibrationData[MemRoutingTable+i*5+5];
    }
    return Completed;
}

bool returnIntToBoolean(uint8_t ChangeInt)
{
    if (ChangeInt == 0x01)
        return true;
    return false;
}

uint8_t returnBooleanToInt(bool ChangeBool)
{
    if (ChangeBool == true)
        return 0x01;
    return 0x00;
}

void ReadAllMemory()
{
    uint8_t i = 0;
    for(i = 0; i <= MemLength; i++)
    {
        simulatedCalibrationData[i] = *(uint8_t*)(i+CALIBRATION_START);
    }
}

uint8_t EraseEntireSector()
{
    MAP_FlashCtl_unprotectSector(FLASH_MAIN_MEMORY_SPACE_BANK1,FLASH_SECTOR31);

    /* Trying to erase the sector. Within this function, the API will
		        automatically try to erase the maximum number of tries. If it fails,
		         trap in an infinite loop */
    if(!MAP_FlashCtl_eraseSector(CALIBRATION_START))
        return ERROR_Erase;
    MAP_FlashCtl_protectSector(FLASH_MAIN_MEMORY_SPACE_BANK1,FLASH_SECTOR31);
    return Completed;
}

void store32bitsToArray(uint32_t integer, uint8_t address)
{
    simulatedCalibrationData[address] = ((integer>>24) & 0xFF);
    simulatedCalibrationData[address+1] = ((integer>>16) & 0xFF);
    simulatedCalibrationData[address+2] = ((integer>>8) & 0xFF);
    simulatedCalibrationData[address+3] = ((integer>>0) & 0xFF);
}

uint8_t read32bitsFromMemory(uint8_t address)
{
    simulatedCalibrationData[address] = *(uint8_t*)(CALIBRATION_START+address);
    simulatedCalibrationData[address+1] = *(uint8_t*)(CALIBRATION_START+address+1);
    simulatedCalibrationData[address+2] = *(uint8_t*)(CALIBRATION_START+address+2);
    simulatedCalibrationData[address+3] = *(uint8_t*)(CALIBRATION_START+address+3);
    return Completed;
}

uint32_t read32bitsFromArray(uint8_t address)
{
    uint32_t tempNumber;
    tempNumber = simulatedCalibrationData[address] <<8;
    tempNumber |= simulatedCalibrationData[address+1];
    tempNumber = tempNumber <<8;
    tempNumber |= simulatedCalibrationData[address+2];
    tempNumber = tempNumber <<8;
    tempNumber |= simulatedCalibrationData[address+3];
    return tempNumber;
}

uint32_t return32bitsFromMemory(uint8_t address)
{
    read32bitsFromMemory(address);
    return read32bitsFromArray(address);
}

