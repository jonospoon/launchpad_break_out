
#include "my_NodeSettings.h"
/*
#include "driverlib.h"
#include "my_externalFlags.h"
#include "timer.h"
#include "ms5607.h"
#include "my_RTC.h"
#include "my_gps.h"
#include "my_uart.h"
#include "my_SystickTimer.h"
#include"ReliableMessage.h"
#include <stdio.h>
#include "MessageRouter.h"
#include "fatfs/src/diskio.h"
#include "fatfs/src/ff.h"
#include "MeshMessage.h"
#include "scheduler.h"
//#include "RadioHead/RH_RF95.h"
#include "RFM96W.h"
 */
void setup_startup(void);
void interrupt_delay(int ms);


bool node1 = false;
bool node2 = false;
bool node3 = false;
bool node4 = false;
bool node5 = false;

/* Application Defines  */
bool LED_on_nOFF;
bool LED_blink = false;
int timer_count = 0;
int32_t temp;
uint32_t pressure;
uint8_t RTC_time [5];
uint16_t year;

#if defined(TARGET_IS_MSP432P4XX)
#define dick 1
#endif

int main(void) 
{
	setup_startup();
	//printAllStats();
	//zeroAllStats();
	/*if (init_rfm96())
	{
		send_UART("RFM96W Init Success\n");
	}*/
	/*if (setupDatagram(CLIENT_ADDRESS))
	{
		send_UART("Datagram Init Success\n");
	}*/
	//Black Route
	//addRouteTo(SERVER_ADDRESS,SERVER_ADDRESS,Valid);

	//FlashWriteAddress(2);
	UpdateRoutingTableFromMemory();

	incrememntNumberRestarts();
	//set_AmIServer(SERVER);
	if(thisAddress()==1)
	{
		//addRouteTo(CLIENT_ADDRESS1,CLIENT_ADDRESS1,Valid,RH_DEFAULT_MAX_HOPS,SERVER);
		addRouteTo(CLIENT_ADDRESS4,CLIENT_ADDRESS2,Valid,2,SERVER);
		//set_AmIServer(SERVER);
		//addRouteTo(CLIENT_ADDRESS4,CLIENT_ADDRESS3,Valid,1,SERVER);
	}


	//Red_Blue Route
	if (thisAddress()==2)
	{
		addRouteTo(CLIENT_ADDRESS4,CLIENT_ADDRESS3,Valid,1,SERVER);
		addRouteTo(CLIENT_ADDRESS1,CLIENT_ADDRESS1,Valid,0,NOT_SERVER);
		addRouteTo(CLIENT_ADDRESS2,CLIENT_ADDRESS2,Valid,1,NOT_SERVER);
		//addRouteTo(CLIENT_ADDRESS4,CLIENT_ADDRESS4,Valid,1,SERVER);
		//set_AmIServer(SERVER);
		//addRouteTo(CLIENT_ADDRESS3,CLIENT_ADDRESS3,Valid,10,NOT_SERVER);
		//addRouteTo(CLIENT_ADDRESS4,CLIENT_ADDRESS4,Valid,20,SERVER);
	}

	//Red_None Route
	if (thisAddress()==3)
	{
		addRouteTo(CLIENT_ADDRESS4,CLIENT_ADDRESS4,Valid,0,SERVER);
		addRouteTo(CLIENT_ADDRESS1,CLIENT_ADDRESS2,Valid,1,NOT_SERVER);
		//addRouteTo(CLIENT_ADDRESS1,CLIENT_ADDRESS3,Valid,10,NOT_SERVER);
		//addRouteTo(CLIENT_ADDRESS2,CLIENT_ADDRESS3,Valid,1,NOT_SERVER);
		//addRouteTo(CLIENT_ADDRESS3,CLIENT_ADDRESS3,Valid,0,NOT_SERVER);
	}
	if (thisAddress()==4)
	{
		//addRouteTo(CLIENT_ADDRESS5,CLIENT_ADDRESS5,Valid,0,SERVER);
		//addRouteTo(CLIENT_ADDRESS1,CLIENT_ADDRESS3,Valid,2,NOT_SERVER);
		//set_AmIServer(SERVER);
	}
	if (node5)
	{
		//addRouteTo(CLIENT_ADDRESS5,CLIENT_ADDRESS5,Valid,0,SERVER);
		addRouteTo(CLIENT_ADDRESS1,CLIENT_ADDRESS4,Valid,3,NOT_SERVER);
	}
	send_UART("My Address is: ");
		send_uart_integer_nextLine(thisAddress());
	printRoutingTable();
	checkForServer();
	//UpdateRoutingTableInMemory();
	//ReadRoutingTableFromMemory();

	//if (AmIServer())
		//send_UART("Im a server\n");

//	send_uart_integer(16);
	//send_uart_integer(256);
	//send_uart_integer(4096);
	/*
	 * GSM!!!! /*** TEST ***/

	//checkGPRSattached();
	// delay_s_Time(200);
	// gsm_ping_google();
	//delay_s_Time(200);
	//checkRegistration();
	//delay_s_Time(10000);
	//gsm_ping_google();
	//HTTP_connect();
	/*
	 * GSM!!!! /*** TEST ***/

	while(1)
	{
		// Toggle P1.0 output
		if (returnMode() == NormalMode)
		{

			if(node1||node2||node3||node4||node5)
			{
				//if(get_batteryOK())
				//schedule();
				//MAP_PCM_gotoLPM0();
				Mesh_Receive_internal();
			}
			Mesh_Receive_internal();
			CheckMemoryAndUpdate();
			checkUartActivity();
			/**** RADIO SEND ***/
			/*

    	    		uint8_t data[] = "Hello World!";
    	    		send_wait_complete(data);


			 */
			//MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
			/**** RADIO RECEIVE ***/
			/*

    	    		if (waitAvailableTimeout(2000))
    	    		{
    	    			// Should be a message for us now
    	    			uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    	    			uint8_t len = sizeof(buf);
    	    			if (recv(buf, &len))
    	    			{
    	    				// digitalWrite(led, HIGH);
    	    				//      RH_RF95::printBuffer("request: ", buf, len);
    	    				send_UART("got request: ");
    	    				send_UART((char*)buf);
    	    				//send_UART("RSSI: ");
    	    				char buffer[40];
    	    				sprintf(buffer,"RSSI: %d \n",lastRssi());
    	    				send_UART(buffer);
    	    				//send_UART(lastRssi());

    	    				// Send a reply

    	    				// rf95.send(data, sizeof(data));
    	    				//rf95.waitPacketSent();
    	    				//Serial.println("Sent a reply");
    	    				// digitalWrite(led, LOW);
    	    				//receives++;
    	    			}
    	    			else
    	    			{
    	    				send_UART("recv failed");
    	    			}
    	    		}
			 */
			/**** RADIO SEND RELIABLE***/
			/*
			uint8_t data[] = "Hello World!";
			send_UART("Sending Message\n");
			if (sendtoWait(data,sizeof(data),SERVER_ADDRESS))
			{
				uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
				uint8_t len = sizeof(buf);
				uint8_t from;
			 /*** RADIO RECEIVE RELIABLE**
				if (recvfromAckTimeout(buf, &len, 4000, &from, CLIENT_ADDRESS,0,0))
				{
					send_UART("got reply from : 0x");
					send_UART(from);
					send_UART(": ");

					char buffer[40];
					sprintf(buffer,"got reply from : 0x%d \nRSSI: %d \n",from,lastRssi());
					send_UART(buffer);
					send_UART((char*)buf);
										send_UART("\n");
				}
				else
				{
					send_UART("No reply, is rf95_reliable_datagram_server running?\n");
				}

			}
			 */
			//setModeIdle();
			//setModeIdle();
			//interrupt_delayms(2000);
			//send_UART("Sending Message\n");
			/** MESSAGE ROUTER */
			/*

			if (black)
			{
				uint8_t data[] = "INTERRUPTING!";
				if( sendtoWait(data,sizeof(data),CLIENT_ADDRESS3, 0)==RH_ROUTER_ERROR_NONE)
				{
					send_UART("Interrupt sent");

					// It has been reliably delivered to the next node.
					// Now wait for a reply from the ultimate server
					uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
					uint8_t len = sizeof(buf);
					uint8_t from;
					uint8_t* dest; uint8_t* id; uint8_t* flags;
					if (recvfromAckTimeoutRoute(buf, &len, 300, &from,&dest, &id, &flags ))
					{
						send_UART("got reply from : 0x");
						send_uart_integer(from);
						send_UART(": ");
						send_UART((char*)buf);
						send_UART("Flags: ");
						send_uart_integer(flags);
						send_UART("\n");
					}
					else
					{
						send_UART("No reply, is rf22_router_server1, rf22_router_server2 and rf22_router_server3 running?");
					}
				}
			}
			if(red||red_None)
			{
				uint8_t data[] = "Hello World from Red!";
				uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
				uint8_t len = sizeof(buf);
				uint8_t from;
				uint8_t* dest; uint8_t* id; uint8_t* flags;
				if (recvfromAckTimeoutRoute(buf, &len, 3000, &from,&dest, &id, &flags ))
				{
					send_UART("got reply from : 0x");
					send_uart_integer(from);
					send_UART(": ");
					send_UART((char*)buf);
					send_UART("\n");
					if(sendtoFromSourceWait(data,sizeof(data),from, thisAddress() ,0)==RH_ROUTER_ERROR_NONE)
					{
						send_UART("Replied \n");
					}

				}
			}
			 */

			//Radio Send MESH

			if(0)
			{
				//uint8_t data[] = "890123456789010123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()";
				uint8_t data[] = "Hello World from Mesh!";
				send_UART("Sending Message\n");
				if (MeshsendtoWait(data, sizeof(data),CLIENT_ADDRESS4,0)== RH_ROUTER_ERROR_NONE)
				{
					// It has been reliably delivered to the next node.
					// Now wait for a reply from the ultimate server
					uint8_t buf[RH_MESH_MAX_MESSAGE_LEN];
					uint8_t len = sizeof(buf);
					uint8_t from;
					uint8_t* dest; uint8_t* id; uint8_t* flags;
					if (MeshrecvfromAckTimeout(buf, &len, 20000, &from,&dest, &id, &flags ))
					{
						send_UART("got reply from : 0x");
						send_uart_integer(from);
						send_UART(": ");
						send_UART((char*)buf);
						send_UART("\nFlags: ");
						send_uart_integer(flags);
						send_UART("\n");
					}
					else
					{
						send_UART("No reply, is rf22_router_server1, rf22_router_server2 and rf22_router_server3 running?\n");
					}
				}
				//printRoutingTable();
				//retireOldestRoute();
				//deleteRoute(1);
			}
			/*
			if(red||red_None)
			{
				uint8_t data[] = "Hello World from Red!";
				uint8_t buf[RH_MESH_MAX_MESSAGE_LEN];
				uint8_t len = sizeof(buf);
				uint8_t from;
				uint8_t dest; uint8_t id; uint8_t flags;
				if (MeshrecvfromAckTimeout(buf, &len, 700, &from,&dest, &id, &flags ))
				{
					send_UART("got reply from : 0x");
					send_uart_integer(from);
					send_UART(": ");
					send_UART((char*)buf);
					send_UART("\n");
					interrupt_delayms(10);
					if(MeshsendtoWait(data,sizeof(data),from, 0)==RH_ROUTER_ERROR_NONE)
					{
						send_UART("Replied \n");
					}

				}
				interrupt_delayms(2200);
				//printRoutingTable();
			}*/

			if(0)
			{
				/*
				int i;
				for (i = 0; i < 50; i++)
					Mesh_Receive_internal();
				 */
				//send_UART("RFM Paused\n");
				//RH_RF95sleep();
				send_UART("RFM Sleep\n");
				//interrupt_delayms(5000);

				//interrupt_delayms(5000);
			}


			/* This code will only run once a day at 23:44
			 * Used to ensure that all clocks are on time
			 * Also can be used to check the network health.
			 */
			/*
			if (daily_update)
			{
				enable_GPS_interrupt();
				startup_GPS();
				newGPS_reading=false;
				while (!newGPS_reading);
				updateTime=true;
				nmea_parse_internal();
				daily_update=false;
				disable_GPS_interrupt();
				shutdown_GPS();
			}*/
			/*
			 * Called every 5 minutes, to check the batery voltage
			 */

			/*if (minute_5_check())
			{
				ADC_checkVoltage();
				set_minute_5_interrupt(false);
			}
			if(minute_check())
			{
				__no_operation();
				//MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
				set_minute_interrupt(false);
			}*/

			/*
    	    		if (newGPS_reading)
    	    		{

    	    			//char * arr;
    	    			//uint8_t length = 10;
    	    			nmea_parse_internal();
    	    			send_UART("GPS GOT");
    	    			newGPS_reading=false;
    	    		}*/


			/**** CHECK AND SET SYSTICK ****/
			/*if(SystimerReadyCheck())
    	    		{
    	    			MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
    	    			runsystickFunction_ms(1000);
    	    		}*/

			if(0 )
			{
				/**** GET TIME FROM RTC ****/
				getTimefromRTCtoDecArray(RTC_time,&year);

				/**** DELAY ***/
				//if (black)
				//interrupt_delayms(5000);


				/**** READ TEMP AND PRESSURE ***/
				temp = MS5607GetTemp();
				//pressure = MS5607GetPressure();

				/**** SEND TO UART ***/

				char buffer[40];
				sprintf(buffer,"The temp is: %d \n"
						"The time is %d:%d:%d %d\n",temp,RTC_time[2],RTC_time[1],RTC_time[0],year);
				send_UART(buffer);
			}
			//MAP_PCM_gotoLPM0();

			/**** WRITE TO SD ***/
			/*
    	    		write_string_toSD(buffer);
			 */

		}

		else if (returnMode() == TestMode)
		{
			checkUartActivity();
			/*
			GPIO_setOutputLowOnPin(
					GPIO_PORT_P1,
					GPIO_PIN0
			);
			send_UART("turned off");
			interrupt_delayms(10000);*/
		}
	}
}



/*
 * Port 1 interrupt handler. This handler is called whenever the switch attached
 * to P1.1 is pressed. A status flag is set to signal for the main application
 * to change power states
 */
void PORT1_IRQHandler(void)
{
	uint32_t status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P1);
	GPIO_clearInterruptFlag(GPIO_PORT_P1, status);


	if (status & GPIO_PIN1)
	{
		LED_on_nOFF = !LED_on_nOFF;
	}
}

void setup_startup(void){
	LED_on_nOFF = true;

	// Stop watchdog timer
	WDT_A_hold(WDT_A_BASE);

	//TerminateGPIO();

	// Set P1.0 to output direction
	GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
	GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0);
	GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN1);

	GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN1);

	/* Confinguring P1.1 as an input and enabling interrupts */
	GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_P1, GPIO_PIN1);
	GPIO_clearInterruptFlag(GPIO_PORT_P1, GPIO_PIN1);
	GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN1);
	GPIO_interruptEdgeSelect(GPIO_PORT_P1, GPIO_PIN1, GPIO_HIGH_TO_LOW_TRANSITION);
	MAP_Interrupt_setPriority(INT_PORT1,0x40);
	Interrupt_enableInterrupt(INT_PORT1);

	/* Confinguring P3.7 as an input and enabling interrupts
	 * Used to detect the VBat_ok signal from the BQ25504*/
	Interrupt_disableInterrupt(INT_PORT3);
	GPIO_setAsInputPin(GPIO_PORT_P3, GPIO_PIN7);
	GPIO_clearInterruptFlag(GPIO_PORT_P3, GPIO_PIN7);
	GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN7);
	GPIO_interruptEdgeSelect(GPIO_PORT_P3, GPIO_PIN7, GPIO_HIGH_TO_LOW_TRANSITION);
	MAP_Interrupt_setPriority(INT_PORT3,0x40);
	Interrupt_enableInterrupt(INT_PORT3);

	/*Set the batteryOK to high*/
	set_batteryOK(true);

	/* Configuring Timer_A1 for Up Mode */
	setup_Timerms_A();

	/* CS setup SD card. */
	GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN2);
	GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN2);

	/*Start GPS UART*/
	setup_GPS_uart();



	//Start the RTC
	RTC_setup_begin();

	//SPI open
	spi_Open();

	/*MS5607 Start*/

	MS5607Reset();
	MS5607Start();

	/*PC Uart interrupt setup*/
	PCuart_setup();

	/*Start GSM UART*/
	GSM_startup();

	/*Read all flash memory*/
	ReadAllMemory();

	/* Resets the UART incoming array*/
	resetUARTArray();

	/*SD card start*/
	mount_sdCard();

	/*Setup Router RFM96W*/
	/*
	if (node1)
	{
		if (setupRouter(CLIENT_ADDRESS1))
			send_UART("Router setup complete\n");
	}
	if(node2)
	{
		if (setupRouter(CLIENT_ADDRESS2))
			send_UART("Router setup complete\n");
	}
	if(node3)
	{
		if (setupRouter(CLIENT_ADDRESS3))
			send_UART("Router setup complete\n");
		//set_AmIServer(SERVER);
	}
	if (node4)
	{
		if (setupRouter(CLIENT_ADDRESS4))
			send_UART("Router setup complete\n");

	}
	if (node5)
	{
		if (setupRouter(CLIENT_ADDRESS5))
			send_UART("Router setup complete\n");
	}
	else
	{*/
		if (setupRouter(FlashReadAdress()))
		{
			send_UART("Radio setup complete\n");
			//set_AmIServer(FlashReadServer());

		}
	//}
	/*Begin SysTick*/
	runsystickFunction_ms(1000);

	Interrupt_disableSleepOnIsrExit();
	//SysCtl_enableSRAMBankRetention(SYSCTL_SRAM_BANK7);
	Interrupt_enableMaster();

	modem_start();
	set_sleep_cycle(true);
}
