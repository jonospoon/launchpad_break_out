/*
 * scheduler.h
 *
 *  Created on: Aug 4, 2017
 *      Author: Jonathan
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "driverlib.h"

void schedule(void);

void set_sleep_cycle(bool setter);
void set_set_receive(bool setter);

#endif /* SCHEDULER_H_ */
