/*
 * my_uart.h
 *
 *  Created on: Apr 12, 2017
 *      Author: Jonathan
 */

#ifndef MY_UART_H_
#define MY_UART_H_
#include <stdint.h>
#include "my_NodeSettings.h"
#define SIZE_BUFFER 80 //Buffer size which UART can receive from PC

/*Send a single character to the UART*/
void send_UART_hex(char bufferHex);
/*Send a char array through the specified UART channel*/
void send_UART(char *buffer);
/*Sets up the uart on P1.2 and P1.3*/
void PCuart_setup(void);

void send_uart_integer(uint32_t integer);

void send_uart_integer_nextLine(uint32_t integer);

void printServerOrNot();

void printTemp();

void printTime();

bool CheckUartBufferServer(uint8_t buffer);

void checkUartActivity();

void UartCommands();

bool returnUartActivity();

void resetUARTArray();

#endif /* MY_UART_H_ */
