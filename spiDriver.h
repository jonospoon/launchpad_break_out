/*
 * spiDriver.h
 *
 *  Created on: Apr 11, 2017
 *      Author: Jonathan
 */

#ifndef SPIDRIVER_H_
#define SPIDRIVER_H_

#define RH_SPI_WRITE_MASK 0x80


/*!
    \brief   type definition for the spi channel file descriptor

    \note    On each porting or platform the type could be whatever is needed
            - integer, pointer to structure etc.
*/
typedef unsigned int Fd_t;


/*!
    \brief open spi communication port to be used for communicating with a
           SimpleLink device

    Given an interface name and option flags, this function opens the spi
    communication port and creates a file descriptor. This file descriptor can
    be used afterwards to read and write data from and to this specific spi
    channel.
    The SPI speed, clock polarity, clock phase, chip select and all other
    attributes are all set to hardcoded values in this function.

    \param[in]      ifName    -    points to the interface name/path. The
                    interface name is an optional attributes that the simple
                    link driver receives on opening the device. in systems that
                    the spi channel is not implemented as part of the os device
                    drivers, this parameter could be NULL.
    \param[in]      flags     -    option flags

    \return         upon successful completion, the function shall open the spi
                    channel and return a non-negative integer representing the
                    file descriptor. Otherwise, -1 shall be returned

    \sa             spi_Close , spi_Read , spi_Write
    \note
    \warning
*/

Fd_t spi_Open(void);

/*!
    \brief closes an opened spi communication port

    \param[in]      fd    -     file descriptor of an opened SPI channel

    \return         upon successful completion, the function shall return 0.
                    Otherwise, -1 shall be returned

    \sa             spi_Open
    \note
    \warning
*/
int spi_Close(void);

/*
 * \brief  Read and write from the SPI bus. sends a single 8 bit of data read from the SPI bus.
 *
 * \param[in]  	pBuff		- 		Points to location to read data from
 *
 * \return		upon successful completetion the function will return the value read from the SPI
*/

int spi_read_write(uint8_t pBuff);


void spi_write(uint8_t addr);

/*
 * \brief  	Read and write a 24 bit uint from the SPI bus. Sends a single 8 bit int which points to the address
 *
 * \param[in]  	addr		- 		Points to location to read data from
 *
 * \return		upon successful completetion the function will return a uint 24 value, which has been read
*/
uint32_t SPIRead24( uint8_t addr );
/*
 *
 */
uint16_t spi_readInt(uint8_t addr);


uint8_t spiRead_RFM(uint8_t addr);
uint8_t spiWrite_RFM(uint8_t reg, uint8_t val);
uint8_t spiBurstRead_RFM(uint8_t reg, uint8_t* dest, uint8_t len);
uint8_t spiBurstWrite_RFM(uint8_t reg, const uint8_t* src, uint8_t len);




#endif /* SPIDRIVER_H_ */
