/*
 * my_sd.h
 *
 *  Created on: Apr 13, 2017
 *      Author: Jonathan
 */
#include "fatfs/src/ff.h"
#ifndef MY_SD_H_
#define MY_SD_H_

void write_string_toSD(char * string);
FRESULT mount_sdCard(void);


#endif /* MY_SD_H_ */
