/*
 * my_powerSettings.h
 *
 *  Created on: Aug 7, 2017
 *      Author: Jonathan
 */

#ifndef MY_POWERSETTINGS_H_
#define MY_POWERSETTINGS_H_


#include "driverlib.h"
/*
 * Sets all devices into sleep mode except the MSP
 */
bool sleep_all(void);
/*
 * Sets all devices to shutdown
 */
bool shutDown_all(void);
/*
 * Disables the ctrl pin on the TPS62742
 * This will cut all power to the SD card, GPS, (GSM)
 * and temp sensor
 */
void disable_ctrl_pin(void);
/*
 * Enables the ctrl pin on the TPS62742, therefore
 * giving power to peripheral sensors.
 */
void enable_ctrl_pin(void);
/*
 * Puts the MSP to sleep
 */
void sleepMSP(void);
/*
 * Sets the MSP into low frequency operation which uses very little
 * power to maintain
 */
void ShutDown_MSP(void);
/*
 * Ramps up the clocks on the MSP back to full capacity
 */
void StartUp_MSP(void);
/*
 * Set the Battery voltageOK monitoring
 */
void set_batteryOK(bool setter);
/*
 * Get BatteryOK signal
 * true = Battery > 3.7v
 * false = Battery < 3.52v
 */
bool get_batteryOK(void);
/*
 * Terminates all GPIO pins to ensure that no pin is mistakenly set high and
 * drawing current
 */
void TerminateGPIO(void); //CURRENTLY NOT WORKING
#endif /* MY_POWERSETTINGS_H_ */
