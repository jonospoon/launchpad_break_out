/*
 * timer.c
 *
 *  Created on: Apr 11, 2017
 *      Author: Jonathan
 */
#include "timer.h"
#include "driverlib.h"

/************** DELAY MS **************/
#define TIMER_PERIOD    0x177
bool timerTick_ms = false;

/************** DELAY MS **************/
/* Timer_A UpMode Configuration Parameter */
const Timer_A_UpModeConfig upConfig =
{
        TIMER_A_CLOCKSOURCE_SMCLK,              // SMCLK Clotimerck Source
		TIMER_A_CLOCKSOURCE_DIVIDER_32,          // SMCLK/1 = 12MHz
        TIMER_PERIOD,                           // 375 tick period
        TIMER_A_TAIE_INTERRUPT_DISABLE,         // Disable Timer interrupt
        TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE ,    // Enable CCR0 interrupt
        TIMER_A_DO_CLEAR                        // Clear value
};
/*
 * Timer 1 interrupt handler. This timer is set to fire at exactly 1ms.
 * Is used most widely by interrupt_delayms() which can then delay for certain periods
 * as needed.
 */
void TA1_0_IRQHandler(void)
{
	timerTick_ms = true;

    MAP_Timer_A_clearCaptureCompareInterrupt(TIMER_A1_BASE,
            TIMER_A_CAPTURECOMPARE_REGISTER_0);
}

void interrupt_delayms(int ms)
{
	int timercount=0;
	MAP_Timer_A_clearTimer(TIMER_A1_BASE);
	MAP_Timer_A_startCounter(TIMER_A1_BASE, TIMER_A_UP_MODE);
	while(timercount<ms)
	{
		if (timerTick_ms)
		{
			timercount++;
			timerTick_ms = false;
		}
	}
	timercount = 0;
	MAP_Timer_A_stopTimer(TIMER_A1_BASE);

}

void setup_Timerms_A()
{
	/* Configuring Timer_A1 for Up Mode */
	MAP_Timer_A_configureUpMode(TIMER_A1_BASE, &upConfig);
	//MAP_Interrupt_setPriority(INT_TA1_0,0x60);
	MAP_Interrupt_enableInterrupt(INT_TA1_0);
}





