/*
 * ReliableMessage.h
 *
 *  Created on: May 23, 2017
 *      Author: Jonathan
 */

#ifndef RELIABLEMESSAGE_H_
#define RELIABLEMESSAGE_H_

#include "RFM96W.h"
#include "my_SystickTimer.h"

// The acknowledgement bit in the FLAGS
// The top 4 bits of the flags are reserved for RadioHead. The lower 4 bits are reserved
// for application layer use.
#define RH_FLAGS_ACK 		0x80
#define FLAGS_CAN_I_TALK 	0x40
#define FLAGS_FROM_A_RHINO 	0x08
#define Node_to_Node		0x04




/// the default retry timeout in milliseconds
#define RH_DEFAULT_TIMEOUT 800

/// The default number of retries
#define RH_DEFAULT_RETRIES 3

// ADRESSES USED FOR DEBUGGING

#define SERVER_ADDRESS 0
#define CLIENT_ADDRESS1 1
#define CLIENT_ADDRESS2 2
#define CLIENT_ADDRESS3 3
#define CLIENT_ADDRESS4 4
#define CLIENT_ADDRESS5 5
#define CLIENT_ADDRESS6 6
#define CLIENT_ADDRESS7 7
#define CLIENT_ADDRESS8 8



bool setupDatagram(uint8_t thisAddress);
void setTimeout(uint16_t timeout);
void setRetries(uint8_t retries);
uint8_t retries();
uint32_t retransmissions();
void resetRetransmissions();
uint32_t returnSendFailures();
uint32_t returnSuccessSend();

bool RFM96W_driver_init(uint8_t thisAddress);

bool sendWithTalkCheck(uint8_t* buf, uint8_t len, uint8_t address);
bool sendtoWait(uint8_t* buf, uint8_t len, uint8_t* address, uint8_t flags);
void acknowledge(uint8_t id, uint8_t from);

bool recvFromAck_CheckTalkFlag(uint8_t* buf, uint8_t* from);
bool recvfromAck(uint8_t* buf, uint8_t* len, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags);
bool recvCheckCanItalk(uint8_t* buf,uint16_t timeout, uint8_t* from);
bool recvfromAckTimeout(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags);

bool sendto(uint8_t* buf, uint8_t len, uint8_t address);

bool recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags);

bool isFromATag(uint8_t flags);




























#endif /* RELIABLEMESSAGE_H_ */
