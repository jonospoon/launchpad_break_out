/*
 * bithacks.h
 *
 *  Created on: Apr 19, 2017
 *      Author: Jonathan
 */

#ifndef BITHACKS_H_
#define BITHACKS_H_

/* set n-th bit in x */
#define B_SET(x, n)      ((x) |= (1<<(n)))

/* unset n-th bit in x */
#define B_UNSET(x, n)    ((x) &= ~(1<<(n)))



#endif /* BITHACKS_H_ */
