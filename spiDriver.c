/*
 * spiDriver.c
 *
 *  Created on: Apr 11, 2017
 *      Author: Jonathan
 */

/* DriverLib Includes */
#include "driverlib.h"
#include "spidriver.h"
#include "bithacks.h"

#define ASSERT_CS()          (P3OUT &= ~BIT6)
#define DEASSERT_CS()        (P3OUT |= BIT6)

/* SPI Master Configuration Parameter */
const eUSCI_SPI_MasterConfig spiMasterConfig =
{
        EUSCI_B_SPI_CLOCKSOURCE_SMCLK,             // SMCLK Clock Source
        3000000,                                   // SMCLK 3Mhz
        500000,                                    // SPICLK = 500khz
        EUSCI_B_SPI_MSB_FIRST,                     // MSB First
        EUSCI_B_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,    // Phase
        EUSCI_B_SPI_CLOCKPOLARITY_INACTIVITY_HIGH, // High polarity
        EUSCI_B_SPI_3PIN                           // 3Wire SPI Mode
};

Fd_t spi_Open(void)
{

    /* Selecting P1.5 P1.6 and P1.7 in SPI mode */
	GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
			GPIO_PIN5 | GPIO_PIN6 | GPIO_PIN7, GPIO_PRIMARY_MODULE_FUNCTION);

	/* CS setup. */
    GPIO_setOutputHighOnPin(GPIO_PORT_P3, GPIO_PIN6);
    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN6);

    /* Configuring SPI in 3wire master mode */
	SPI_initMaster(EUSCI_B0_BASE, &spiMasterConfig);

	/* Enable SPI module */
	SPI_enableModule(EUSCI_B0_BASE);

    return 0;//NONOS_RET_OK;
}

int spi_Close(void)
{
    /* Disable WLAN Interrupt ... */
    //CC3100_InterruptDisable();
	SPI_disableModule(EUSCI_B0_BASE);
    return 0;//NONOS_RET_OK;
}

int spi_read_write(uint8_t pBuff)
{
	int RXData;
	while (!(SPI_getInterruptStatus(EUSCI_B0_BASE, EUSCI_B_SPI_TRANSMIT_INTERRUPT)));
	/* Transmitting data to slave */
	SPI_transmitData(EUSCI_B0_BASE, pBuff);
	while (!(SPI_getInterruptStatus(EUSCI_B0_BASE, EUSCI_B_SPI_TRANSMIT_INTERRUPT)));
	RXData = SPI_receiveData(EUSCI_B0_BASE);

	return RXData;
}

void spi_write(uint8_t addr)
{
	while (!(SPI_getInterruptStatus(EUSCI_B0_BASE, EUSCI_B_SPI_TRANSMIT_INTERRUPT)));
		/* Transmitting data to slave */
		SPI_transmitData(EUSCI_B0_BASE, addr);
}

uint32_t SPIRead24( uint8_t addr )
{
	uint32_t value = 0;
	spi_read_write(addr);
	value = spi_read_write(0);
	value = value << 8;
	value |= spi_read_write(0);
	value = value << 8;
	value |= spi_read_write(0);
	return value;
}

uint16_t spi_readInt(uint8_t addr){
		uint16_t value;
		uint8_t temp_value;
		temp_value = spi_read_write(addr);
		value = spi_read_write(0);
		value = value << 8;
		value += spi_read_write(0);
		return value;
}

#define 	RFM96W_CS_OUT			P5OUT
#define 	RFM96W_CS_BIT			1

#define RFM96W_CS_ENABLE	B_UNSET(RFM96W_CS_OUT, RFM96W_CS_BIT)
#define RFM96W_CS_DISABLE	B_SET(	RFM96W_CS_OUT, RFM96W_CS_BIT)


uint8_t spiRead_RFM(uint8_t addr)
{
	uint8_t val;
	RFM96W_CS_ENABLE;
	uint8_t value_to_send = addr& ~RH_SPI_WRITE_MASK;
	val = spi_read_write(addr& ~RH_SPI_WRITE_MASK);
	val = spi_read_write(0);
	RFM96W_CS_DISABLE;
	return val;
}

uint8_t spiWrite_RFM(uint8_t reg, uint8_t val)
{
    uint8_t status = 0;
    //ATOMIC_BLOCK_START;
    RFM96W_CS_ENABLE;
    uint8_t value_to_send = reg | RH_SPI_WRITE_MASK;
    status = spi_read_write(reg | RH_SPI_WRITE_MASK); // Send the address with the write mask on
    spi_read_write(val); // New value follows
    RFM96W_CS_DISABLE;
    //ATOMIC_BLOCK_END;
    return status;
}

uint8_t spiBurstRead_RFM(uint8_t reg, uint8_t* dest, uint8_t len)
{
    uint8_t status = 0;
    //ATOMIC_BLOCK_START;
    RFM96W_CS_ENABLE;
    status = spi_read_write(reg & ~RH_SPI_WRITE_MASK); // Send the start address with the write mask off
    while (len--)
	*dest++ = spi_read_write(0);
    RFM96W_CS_DISABLE;
   // ATOMIC_BLOCK_END;
    return status;
}

uint8_t spiBurstWrite_RFM(uint8_t reg, const uint8_t* src, uint8_t len)
{
    uint8_t status = 0;
    //ATOMIC_BLOCK_START;
    RFM96W_CS_ENABLE;
    status = spi_read_write(reg | RH_SPI_WRITE_MASK); // Send the start address with the write mask on
    while (len--)
    	spi_read_write(*src++);
    RFM96W_CS_DISABLE;
    //ATOMIC_BLOCK_END;
    return status;
}

