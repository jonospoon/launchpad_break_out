/*
 * my_externalFlags.h
 *
 *  Created on: May 18, 2017
 *      Author: Jonathan
 */

#ifndef MY_EXTERNALFLAGS_H_
#define MY_EXTERNALFLAGS_H_


extern bool newGPS_reading;
extern bool updateTime;
extern bool daily_update;
extern bool minute_interrupt;

#endif /* MY_EXTERNALFLAGS_H_ */
