/*
 * my_ADC.h
 *
 *  Created on: Aug 1, 2017
 *      Author: Jonathan
 */

#ifndef MY_ADC_H_
#define MY_ADC_H_

#include "driverlib.h"
/*
 * ADC_setup: is used to set up the timer
 * and registers used for the ADC. At current only pin5.5 is measured
 */
void ADC_setup(void);
/*
 * Starts the conversion which will take 10ms to complete for 10 readings
 * Each conversion takes 1ms and is stored in a buffer
 */
void ADC_startConversion(void);
/*
 * Calculates the average value from 10 readings stored in the buffer
 */
uint16_t ADC_calculate_average_fromBuffer(void);
/*
 * Used to check the averages from the adc to determine what voltage the battery
 * currently holds.
 */
uint8_t ADC_checkVoltage(void);



#endif /* MY_ADC_H_ */
