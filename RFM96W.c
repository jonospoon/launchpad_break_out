/*
 * RFM96W.c
 *
 *  Created on: Apr 19, 2017
 *      Author: Jonathan
 */
#include "driverlib.h"
#include "spidriver.h"
#include "RFM96W.h"
#include "timer.h"
#include "stdlib.h"
#include "my_uart.h"
#include "my_SystickTimer.h"
#include <string.h>

/// Number of octets in the buffer
volatile uint8_t    _bufLen;

/// The receiver/transmitter buffer
uint8_t             _buf[RH_RF95_MAX_PAYLOAD_LEN];

/// True when there is a valid message in the buffer
volatile bool       _rxBufValid;

// True if we are using the HF port (779.0 MHz and above)
bool                _usingHFport;

// Last measured SNR, dB
int8_t              _lastSNR;

/// The current transport operating mode
volatile RHMode     _mode;

/// This node id
uint8_t             _thisAddress;

/// Whether the transport is in promiscuous mode
bool                _promiscuous;

/// TO header in the last received mesasge
volatile uint8_t    _rxHeaderTo;

/// FROM header in the last received mesasge
volatile uint8_t    _rxHeaderFrom;

/// ID header in the last received mesasge
volatile uint8_t    _rxHeaderId;

/// FLAGS header in the last received mesasge
volatile uint8_t    _rxHeaderFlags;

/// TO header to send in all messages
uint8_t             _txHeaderTo;

/// FROM header to send in all messages
uint8_t             _txHeaderFrom;

/// ID header to send in all messages
uint8_t             _txHeaderId;

/// FLAGS header to send in all messages
uint8_t             _txHeaderFlags;

/// The value of the last received RSSI value, in some transport specific units
volatile int8_t     _lastRssi;

/// Count of the number of bad messages (eg bad checksum etc) received
volatile uint16_t   _rxBad;

/// Count of the number of successfully transmitted messaged
volatile uint16_t   _rxGood;

/// Count of the number of bad messages (correct checksum etc) received
volatile uint16_t   _txGood;

/// Channel activity detected
volatile bool       _cad;

/// Channel activity timeout in ms
unsigned int        _cad_timeout;

static const ModemConfig MODEM_CONFIG_TABLE[] =
{
		//  1d,     1e,      26
		{ 0x72,   0x74,    0x00}, // Bw125Cr45Sf128 (the chip default)
		{ 0x92,   0x74,    0x00}, // Bw500Cr45Sf128
		{ 0x48,   0x94,    0x00}, // Bw31_25Cr48Sf512
		{ 0x78,   0xc4,    0x00}, // Bw125Cr48Sf4096

};


bool init_rfm96()
{
	//if (!RHSPIDriver::init())
	//return false;
	//enable the module
	MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P4, GPIO_PIN6);
	MAP_GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN6);
	// Determine the interrupt number that corresponds to the interruptPin
	//int interruptNumber = digitalPinToInterrupt(_interruptPin);
	//if (interruptNumber == NOT_AN_INTERRUPT)
	//return false;
	/* CS setup. */
	GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN1);
	GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN1);

	// No way to check the device type :-(

	// Set sleep mode, so we can also set LORA mode:
	spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_SLEEP | RH_RF95_LONG_RANGE_MODE);
	interrupt_delayms(100); // Wait for sleep mode to take over from say, CAD
	// Check we are in sleep mode, with LORA set
	if (spiRead_RFM(RH_RF95_REG_01_OP_MODE) != (RH_RF95_MODE_SLEEP | RH_RF95_LONG_RANGE_MODE))
	{
		//	Serial.println(spiRead(RH_RF95_REG_01_OP_MODE), HEX);
		return false; // No device present?
	}

	// Add by Adrien van den Bossche <vandenbo@univ-tlse2.fr> for Teensy
	// ARM M4 requires the below. else pin interrupt doesn't work properly.
	// On all other platforms, its innocuous, belt and braces
	//pinMode(_interruptPin, INPUT);

	// Set up interrupt handler
	// Since there are a limited number of interrupt glue functions isr*() available,
	// we can only support a limited number of devices simultaneously
	// ON some devices, notably most Arduinos, the interrupt pin passed in is actuallt the
	// interrupt number. You have to figure out the interruptnumber-to-interruptpin mapping
	// yourself based on knwledge of what Arduino board you are running on.


	GPIO_setAsInputPinWithPullDownResistor(GPIO_PORT_P4, GPIO_PIN1);
	GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN1);
	GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN1);
	GPIO_interruptEdgeSelect(GPIO_PORT_P4, GPIO_PIN1, GPIO_LOW_TO_HIGH_TRANSITION);
	//MAP_Interrupt_setPriority(INT_PORT4,0x20);
	Interrupt_enableInterrupt(INT_PORT4);

	setCADTimeout(RH_CAD_DEFAULT_TIMEOUT);

	// Set up FIFO
	// We configure so that we can use the entire 256 byte FIFO for either receive
	// or transmit, but not both at the same time
	spiWrite_RFM(RH_RF95_REG_0E_FIFO_TX_BASE_ADDR, 0);
	spiWrite_RFM(RH_RF95_REG_0F_FIFO_RX_BASE_ADDR, 0);

	// Packet format is preamble + explicit-header + payload + crc
	// Explicit Header Mode
	// payload is TO + FROM + ID + FLAGS + message data
	// RX mode is implmented with RXCONTINUOUS
	// max message data length is 255 - 4 = 251 octets

	setModeIdle();

	// Set up default configuration
	// No Sync Words in LORA mode.
	//setModemConfig(Bw125Cr45Sf128); // Radio default
	setModemConfig(Bw31_25Cr48Sf512);
	//setModemConfig(Bw500Cr45Sf128);
	//setModemConfig(Bw125Cr48Sf4096); // slow and reliable?
	setPreambleLength(8); // Default is 8
	// An innocuous ISM frequency, same as RF22's
	setFrequency(434.0);
	// Lowish power
	setTxPower(20,false);

	return true;
}

void clearBuffers()
{
	spiWrite_RFM(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
	spiWrite_RFM(RH_RF95_REG_0E_FIFO_TX_BASE_ADDR, 0);
	spiWrite_RFM(RH_RF95_REG_0F_FIFO_RX_BASE_ADDR, 0);
	spiWrite_RFM(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
}

bool wakeupRFM()
{
	spiWrite_RFM(RH_RF95_REG_01_OP_MODE,RH_RF95_MODE_RXSINGLE);
	/*
	if (_mode != RHModeSleep)
	{
		return false;
	}
	if (spiRead_RFM(RH_RF95_REG_01_OP_MODE) != (RH_RF95_MODE_SLEEP | RH_RF95_LONG_RANGE_MODE))
	{
		//	Serial.println(spiRead(RH_RF95_REG_01_OP_MODE), HEX);
		return false; // No device present?
	}

	*/
}

// LORA is unusual in that it has several interrupt lines, and not a single, combined one.
// On MiniWirelessLoRa, only one of the several interrupt lines (DI0) from the RFM95 is usefuly
// connnected to the processor.
// We use this to get RxDone and TxDone interrupts
void PORT4_IRQHandler()
{
	uint32_t status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P4);
	//spiWrite_RFM(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
	/*GPIO_toggleOutputOnPin
	     	    		    	    							(
	     	    		    	    									GPIO_PORT_P1,
	     	    		    	    									GPIO_PIN0
	     	    		    	    							);*/

	// Read the interrupt register
	uint8_t irq_flags = spiRead_RFM(RH_RF95_REG_12_IRQ_FLAGS);
	spiWrite_RFM(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags
	GPIO_clearInterruptFlag(GPIO_PORT_P4, status);
	//GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN1);
	if (_mode == RHModeRx && irq_flags & (RH_RF95_RX_TIMEOUT | RH_RF95_PAYLOAD_CRC_ERROR))
	{
		_rxBad++;
	}
	else if (_mode == RHModeRx && irq_flags & RH_RF95_RX_DONE)
	{
		// Have received a packet
		uint8_t len = spiRead_RFM(RH_RF95_REG_13_RX_NB_BYTES);

		// Reset the fifo read ptr to the beginning of the packet
		spiWrite_RFM(RH_RF95_REG_0D_FIFO_ADDR_PTR, spiRead_RFM(RH_RF95_REG_10_FIFO_RX_CURRENT_ADDR));
		spiBurstRead_RFM(RH_RF95_REG_00_FIFO, _buf, len);
		_bufLen = len;
		spiWrite_RFM(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags

		// Remember the RSSI of this packet
		// this is according to the doc, but is it really correct?
		// weakest receiveable signals are reported RSSI at about -66
		_lastRssi = spiRead_RFM(RH_RF95_REG_1A_PKT_RSSI_VALUE) - 137;

		// We have received a message.
		validateRxBuf();
		if (_rxBufValid)
			setModeIdle(); // Got one
	}
	else if (_mode == RHModeTx && irq_flags & RH_RF95_TX_DONE)
	{
		_txGood++;
		setModeIdle();
	}
	else if (_mode == RHModeCad && irq_flags & RH_RF95_CAD_DONE)
	{
		_cad = irq_flags & RH_RF95_CAD_DETECTED;
		setModeIdle();
	}
	spiWrite_RFM(RH_RF95_REG_12_IRQ_FLAGS, 0xff); // Clear all IRQ flags



}

// Check whether the latest received message is complete and uncorrupted
void validateRxBuf()
{
	if (_bufLen < 4)
		return; // Too short to be a real message
	// Extract the 4 headers
	_rxHeaderTo    = _buf[0];
	_rxHeaderFrom  = _buf[1];
	_rxHeaderId    = _buf[2];
	_rxHeaderFlags = _buf[3];
	if (_promiscuous ||
			_rxHeaderTo == _thisAddress ||
			_rxHeaderTo == RH_BROADCAST_ADDRESS)
	{
		_rxGood++;
		_rxBufValid = true;
	}
}


void setModeIdle()
{
	if (_mode != RHModeIdle)
	{
		spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_STDBY);
		spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_STDBY);
		_mode = RHModeIdle;
	}
}

// Set one of the canned FSK Modem configs
// Returns true if its a valid choice
bool setModemConfig(ModemConfigChoice index)
{
	if (index > (signed int)(sizeof(MODEM_CONFIG_TABLE) / sizeof(ModemConfig)))
		return false;

	ModemConfig cfg;
	memcpy(&cfg, &MODEM_CONFIG_TABLE[index], sizeof(ModemConfig));
	setModemRegisters(&cfg);

	return true;
}
// Sets registers from a canned modem configuration structure
void setModemRegisters(const ModemConfig* config)
{
	//uint8_t val = config->reg_1d;
	//val = config->reg_1e;
	spiWrite_RFM(RH_RF95_REG_1D_MODEM_CONFIG1,       config->reg_1d);
	spiWrite_RFM(RH_RF95_REG_1E_MODEM_CONFIG2,       config->reg_1e);
	spiWrite_RFM(RH_RF95_REG_26_MODEM_CONFIG3,       config->reg_26);
}

void setPreambleLength(uint16_t bytes)
{
	spiWrite_RFM(RH_RF95_REG_20_PREAMBLE_MSB, bytes >> 8);
	spiWrite_RFM(RH_RF95_REG_21_PREAMBLE_LSB, bytes & 0xff);
}

void setTxPower(int8_t power, bool useRFO)
{
	if (power > 20)
		power = 20;
	if (power < 5)
		power = 5;
	if (power >= 20)
	{
		spiWrite_RFM(RH_RF95_REG_4D_PA_DAC, RH_RF95_PA_DAC_ENABLE);
		//power -= 3;
	}
	else
	{
		spiWrite_RFM(RH_RF95_REG_4D_PA_DAC, RH_RF95_PA_DAC_DISABLE);
	}
	// RFM95/96/97/98 does not have RFO pins connected to anything. ONly PA_BOOST
	// pin is connected, so must use PA_BOOST
	// Pout = 2 + OutputPower.
	// The documentation is pretty confusing on this topic: PaSelect says the max poer is 20dBm,
	// but OutputPower claims it would be 17dBm.
	// My measurements show 20dBm is correct
	spiWrite_RFM(RH_RF95_REG_09_PA_CONFIG, RH_RF95_PA_SELECT | (power-5));
	//    spiWrite(RH_RF95_REG_09_PA_CONFIG, 0); // no power
}

bool setFrequency(float centre)
{
	// Frf = FRF / FSTEP
	uint32_t frf = (centre * 1000000.0) / RH_RF95_FSTEP;
	spiWrite_RFM(RH_RF95_REG_06_FRF_MSB, (frf >> 16) & 0xff);
	spiWrite_RFM(RH_RF95_REG_07_FRF_MID, (frf >> 8) & 0xff);
	spiWrite_RFM(RH_RF95_REG_08_FRF_LSB, frf & 0xff);

	return true;
}

bool send(const uint8_t* data, uint8_t len)
{
	if (len > RH_RF95_MAX_MESSAGE_LEN)
		return false;

	waitPacketSent(); // Make sure we dont interrupt an outgoing message
	setModeIdle();
	//MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0); /*#### TEST ####*/
	if (!waitCAD())
		return false;  // Check channel activity
	MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0); /*#### TEST ####*/
	// Position at the beginning of the FIFO
	spiWrite_RFM(RH_RF95_REG_0D_FIFO_ADDR_PTR, 0);
	// The headers
	//uint8_t val = _txHeaderTo;
	// _txHeaderTo = 255;
	// _txHeaderFrom = 255;
	spiWrite_RFM(RH_RF95_REG_00_FIFO, _txHeaderTo);
	spiWrite_RFM(RH_RF95_REG_00_FIFO, _txHeaderFrom);
	spiWrite_RFM(RH_RF95_REG_00_FIFO, _txHeaderId);
	spiWrite_RFM(RH_RF95_REG_00_FIFO, _txHeaderFlags);
	// The message data
	spiBurstWrite_RFM(RH_RF95_REG_00_FIFO, data, len);
	spiWrite_RFM(RH_RF95_REG_22_PAYLOAD_LENGTH, len + RH_RF95_HEADER_LEN);

	setModeTx(); // Start the transmitter
	// when Tx is done, interruptHandler will fire and radio mode will return to STANDBY
	return true;
}
bool send_wait_complete(const uint8_t* data)
{
	bool sent = send(data, strlen(data));
	waitPacketSent();
	return sent;
}

bool available()
{
	if (_mode == RHModeTx)
		return false;
	setModeRx();
	return _rxBufValid; // Will be set by the interrupt handler when a good message is received
}

bool recv(uint8_t* buf, uint8_t* len)
{
	if (!available())
		return false;
	if (buf && len)
	{
		//ATOMIC_BLOCK_START;
		// Skip the 4 headers that are at the beginning of the rxBuf
		if (*len > _bufLen-RH_RF95_HEADER_LEN)
			*len = _bufLen-RH_RF95_HEADER_LEN;
		memcpy(buf, _buf+RH_RF95_HEADER_LEN, *len);
		//ATOMIC_BLOCK_END;
	}
	clearRxBuf(); // This message accepted and cleared
	return true;
}

void setModeRx()
{
	if (_mode != RHModeRx)
	{
		//uint8_t read = (spiRead_RFM(RH_RF95_REG_01_OP_MODE)& 0xE3)|RH_RF95_MODE_RXCONTINUOUS;
		spiWrite_RFM(RH_RF95_REG_01_OP_MODE, (spiRead_RFM(RH_RF95_REG_01_OP_MODE) & 0xE3)|RH_RF95_MODE_RXCONTINUOUS);
		//spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_RXCONTINUOUS);
		//spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_RXSINGLE); //Change to this if it is just for a node who wants to receive and then go to sleep
		spiWrite_RFM(RH_RF95_REG_40_DIO_MAPPING1, 0x00); // Interrupt on RxDone
		spiWrite_RFM(RH_RF95_REG_40_DIO_MAPPING1, 0x00); // Interrupt on RxDone
		_mode = RHModeRx;
	}
}
void clearRxBuf()
{
	//ATOMIC_BLOCK_START;
	_rxBufValid = false;
	_bufLen = 0;
	//ATOMIC_BLOCK_END;
}

bool waitAvailableTimeout(uint16_t timeout)
{

	while (timeout > 0)
	{
		if (available())
		{
			return true;
		}
		interrupt_delayms(1);
		timeout--;
	}
	return false;
}

bool waitPacketSent()
{

	while (_mode == RHModeTx)
		__no_operation();
	// interrupt_delayms(1500); // Wait for any previous transmit to finish
	return true;
}

int8_t lastRssi()
{
	return _lastRssi;
}

bool waitCAD()
{
	if (!_cad_timeout)
		return true;

	// Wait for any channel activity to finish or timeout
	// Sophisticated DCF function...
	// DCF : BackoffTime = random() x aSlotTime
	// 100 - 1000 ms
	// 10 sec timeout


	uint16_t timeout =  _cad_timeout + (rand()%_cad_timeout);
	runsystickFunction_ms(timeout);
	while (isChannelActive())
	{
		if (SystimerReadyCheck())
			return false;
		//Wait of between 100ms and 400ms before next CAD check
		interrupt_delayms(((rand()%4)+1)*RH_CAD_WAIT_Multiple);
	}

	return true;
}
bool isChannelActive()
{
	// Set mode RHModeCad
	if (_mode != RHModeCad)
	{
		spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_CAD);
		spiWrite_RFM(RH_RF95_REG_40_DIO_MAPPING1, 0x80); // Interrupt on CadDone
		spiWrite_RFM(RH_RF95_REG_40_DIO_MAPPING1, 0x80); // Interrupt on CadDone
		_mode = RHModeCad;
	}

	while (_mode == RHModeCad)
		__no_operation();

	return _cad;
}

void setModeTx()
{
	if (_mode != RHModeTx)
	{
		spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_TX);
		spiWrite_RFM(RH_RF95_REG_40_DIO_MAPPING1, 0x40); // Interrupt on TxDone
		spiWrite_RFM(RH_RF95_REG_40_DIO_MAPPING1, 0x40); // Interrupt on TxDone
		_mode = RHModeTx;
	}
}

bool RH_RF95sleep()
{
	if (_mode != RHModeSleep)
	{
		spiWrite_RFM(RH_RF95_REG_01_OP_MODE, (spiRead_RFM(RH_RF95_REG_01_OP_MODE) & 0xE3) | RH_RF95_MODE_SLEEP);
		//spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_SLEEP| RH_RF95_LONG_RANGE_MODE);
		//spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_SLEEP | RH_RF95_LONG_RANGE_MODE);
		//interrupt_delayms(100);
		//spiWrite_RFM(RH_RF95_REG_01_OP_MODE, RH_RF95_MODE_SLEEP | RH_RF95_LONG_RANGE_MODE);
		_mode = RHModeSleep;
	}
	return true;
}

void startup_RFM96W(void)
{
	MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P4, GPIO_PIN6);
	interrupt_delayms(10);
	init_rfm96();
}

void shutdown_RFM96W(void) //Requires a full restart
{
	RH_RF95sleep();

	MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN6);
}
void setCADTimeout(uint16_t cad_timeout)
{
	_cad_timeout = cad_timeout;
}

void setRFMAddress(uint8_t address)
{
	_thisAddress = address;
}

void setRFMHeaderTo(uint8_t to)
{
	_txHeaderTo = to;
}
void setRFMHeaderFrom(uint8_t from)
{
	_txHeaderFrom = from;
}
void setRFMHeaderID(uint8_t id)
{
	_txHeaderId = id;
}
void setRFMHeaderFlags(uint8_t set, uint8_t clear)
{
	_txHeaderFlags &= ~clear;
	_txHeaderFlags |= set;
}

void setAddress(uint8_t thisAddress)
{
	setRFMAddress(thisAddress);
	// Use this address in the transmitted FROM header
	setRFMHeaderFrom(thisAddress);
	_thisAddress = thisAddress;
}

uint8_t headerTo()
{
	return _rxHeaderTo;
}

uint8_t headerFrom()
{
	return _rxHeaderFrom;
}

uint8_t headerId()
{
	return _rxHeaderId;
}

uint8_t headerFlags()
{
	return _rxHeaderFlags;
}

uint8_t myHeaderFlags()
{
	return _txHeaderFlags;
}

uint8_t thisAddress()
{
	return _thisAddress;
}

/*bool isFromaTag()
{
	if (headerTo()==RH_BROADCAST_ADDRESS && flags& 0x08)
	{
		return true;
	}
	return false;
}*/
