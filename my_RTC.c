/*
 * my_RTC.c
 *
 *  Created on: May 16, 2017
 *      Author: Jonathan
 */

#include "my_RTC.h"

static volatile RTC_C_Calendar newTime;
bool daily_update = false;
bool minute_interrupt = false;
bool minute_5_interrupt = false;
uint8_t minutes = 0;
/* Time is November 12th 1955 15:32:56 PM */
RTC_C_Calendar oldTime =
{
		0x46, 	//46 seconds
		0x43,	//43 minutes
		0x23,	//23 hours
		0x01,	//1 day of the week
		0x16,	//16 day of the month
		0x05,	//5 months
		0x1955	//1955 years
};


/* RTC ISR */
void RTC_C_IRQHandler(void)
{
	uint32_t status;

	status = MAP_RTC_C_getEnabledInterruptStatus();
	MAP_RTC_C_clearInterruptFlag(status);

	if (status & RTC_C_CLOCK_READ_READY_INTERRUPT)
	{
		//MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
	}

	if (status & RTC_C_TIME_EVENT_INTERRUPT)
	{
		//updateTime=true; //Move this out of here later******************
		minute_interrupt=true;
		minutes++;
		if (minutes%5==0)
		{
			minute_5_interrupt=true;
		}
		/* Interrupts every hour - Set breakpoint here */
		__no_operation();
	}

	if (status & RTC_C_CLOCK_ALARM_INTERRUPT)
	{

		/* Interrupts at a specific time once a day (Set to 23:44) */
		daily_update= true;
		// __no_operation();
	}

}
bool minute_5_check(void)
{
	return minute_5_interrupt;
}

bool minute_check(void)
{
	return minute_interrupt;
}
void set_minute_5_interrupt(bool set)
{
	minute_5_interrupt = set;
}
void set_minute_interrupt(bool set)
{
	minute_interrupt= set;
}

void set_RTCfromGPS(void)
{
	enable_GPS_interrupt();
	startup_GPS();
	newGPS_reading=false;
	if(wait_for_NEWreading(50)) //Wait 2 seconds for a new GPS reading
	{
		updateTime=true;
		nmea_parse_internal();
		daily_update=false;
	}
	else
	{
		send_UART("No Sattellite connection\n");
	}
		disable_GPS_interrupt();
		shutdown_GPS();
}

void RTC_setup_begin()
{
	/* Configuring pins for peripheral/crystal usage and LED for output */
	MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_PJ,
			GPIO_PIN0 | GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);

	/* Setting the external clock frequency. This API is optional, but will
	 * come in handy if the user ever wants to use the getMCLK/getACLK/etc
	 * functions
	 */
	MAP_CS_setExternalClockSourceFrequency(32000,48000000);
	/* Starting LFXT in non-bypass mode without a timeout. */
	MAP_CS_startLFXT(false);
	RTC_setTime(oldTime);
	//MAP_RTC_C_initCalendar(&currentTime, RTC_C_FORMAT_BCD);

	/***** TEST */
	RTC_C_Calendar currenttime;
	currenttime = RTC_C_getCalendarTime_mine();
	/***** TEST */
	/* Enable interrupt for RTC Ready Status, which asserts when the RTC
	 * Calendar registers are ready to read.
	 * Also, enable interrupts for the Calendar alarm and Calendar event. */
	MAP_RTC_C_clearInterruptFlag(
			RTC_C_CLOCK_READ_READY_INTERRUPT | RTC_C_TIME_EVENT_INTERRUPT
			| RTC_C_CLOCK_ALARM_INTERRUPT);
	MAP_RTC_C_enableInterrupt(
			RTC_C_CLOCK_READ_READY_INTERRUPT | RTC_C_TIME_EVENT_INTERRUPT
			| RTC_C_CLOCK_ALARM_INTERRUPT);
	/* Sets a interrupt for every minute*/
	MAP_RTC_C_setCalendarEvent(RTC_C_CALENDAREVENT_MINUTECHANGE);
	/* Sets an alarm interrupt for 23:44 each day*/
	MAP_RTC_C_setCalendarAlarm(0x44, 0x23, RTC_C_ALARMCONDITION_OFF,
			RTC_C_ALARMCONDITION_OFF);
	/* Start RTC Clock */
	RTC_begin();
	/* Enable interrupts. */
	MAP_Interrupt_enableInterrupt(INT_RTC_C);
	set_RTCfromGPS();
}


RTC_C_Calendar RTC_C_getCalendarTime_mine(void)
{
    RTC_C_Calendar tempCal;

    while (!(BITBAND_PERI(RTC_C->CTL13, RTC_C_CTL13_RDY_OFS)))
        ;

    //tempCal.seconds = RTC_C->TIM0 & RTC_C_TIM0_SEC_MASK;
    //tempCal.minutes = (RTC_C->TIM0 & RTC_C_TIM0_MIN_MASK)>>RTC_C_TIM0_MIN_OFS;
    //tempCal.hours   = RTC_C->TIM1 & RTC_C_TIM1_HOUR_MASK;
    tempCal.seconds = RTC_C->TIM0 & 0xFF;
    tempCal.minutes = (RTC_C->TIM0 & 0xFF00)>>RTC_C_TIM0_MIN_OFS;
    tempCal.hours   = RTC_C->TIM1 & 0x3F;
    tempCal.dayOfWeek   = (RTC_C->TIM1 & RTC_C_TIM1_DOW_MASK)>>RTC_C_TIM1_DOW_OFS;
    tempCal.dayOfmonth = RTC_C->DATE & RTC_C_DATE_DAY_MASK;
    tempCal.month = (RTC_C->DATE & RTC_C_DATE_MON_MASK)>>RTC_C_DATE_MON_OFS;
    tempCal.year = RTC_C->YEAR;

    return (tempCal);
}

void RTC_setTime(RTC_C_Calendar currentTime)
{
	MAP_RTC_C_holdClock();
	//ROM_RTC_C_initCalendar(&newtime, RTC_C_FORMAT_BCD);
	RTC_C->CTL0 = (RTC_C->CTL0 & ~RTC_C_CTL0_KEY_MASK) | RTC_C_KEY;

	    BITBAND_PERI(RTC_C->CTL13, RTC_C_CTL13_HOLD_OFS) = 1;

	    BITBAND_PERI(RTC_C->CTL13, RTC_C_CTL13_BCD_OFS) = 1;

	    RTC_C->TIM0 = (currentTime.minutes << RTC_C_TIM0_MIN_OFS)
	            | currentTime.seconds;
	    __no_operation();
	    RTC_C->TIM1 = (currentTime.dayOfWeek << RTC_C_TIM1_DOW_OFS)
	            | currentTime.hours;
	    __no_operation();
	    RTC_C->DATE = (currentTime.month << RTC_C_DATE_MON_OFS)
	            | currentTime.dayOfmonth;
	    __no_operation();
	    RTC_C->YEAR = currentTime.year;

	    BITBAND_PERI(RTC_C->CTL0, RTC_C_CTL0_KEY_OFS) = 0;
	    __no_operation();
	MAP_RTC_C_startClock();
}

RTC_C_Calendar RTC_getTime()
{
	RTC_C_Calendar currenttime;
	currenttime = RTC_C_getCalendarTime_mine();
	return currenttime;
}

void RTC_begin()
{
	MAP_RTC_C_startClock();
}

uint8_t dec2BCD(uint8_t val)
{
	return ((val/10*16)+(val%10));
}
uint8_t BCD2Dec(uint8_t val)
{
	return ((val/16*10)+(val%16));
}
void setTimefromDecArray(uint8_t* timeArr, uint16_t year)
{

	newTime.seconds = dec2BCD(timeArr[0]);
	newTime.minutes = dec2BCD(timeArr[1]);
	newTime.hours = dec2BCD(timeArr[2]);
	newTime.dayOfmonth = dec2BCD(timeArr[3]);
	newTime.month = dec2BCD(timeArr[4]);
	uint8_t topYear = dec2BCD(year/100);
	uint8_t botYear = dec2BCD(year%100);
	newTime.year = (topYear<<8)|(botYear);

	RTC_setTime(newTime);
}
void setTimefromDecCalander(RTC_C_Calendar *time)
{

	newTime.seconds = dec2BCD(time->seconds);
	newTime.minutes = dec2BCD(time->minutes);
	newTime.hours = dec2BCD(time->hours);
	newTime.dayOfmonth = dec2BCD(time->dayOfmonth);
	newTime.month = dec2BCD(time->month);
	uint8_t topYear = dec2BCD(time->year/100);
	uint8_t botYear = dec2BCD(time->year%100);
	newTime.year = (topYear<<8)|(botYear);

	RTC_setTime(newTime);
}

void getTimefromRTCtoDecArray(uint8_t* timeArr, uint16_t *year)
{
	newTime = RTC_C_getCalendarTime_mine();
	timeArr[0] = BCD2Dec(newTime.seconds);
	timeArr[1] = BCD2Dec(newTime.minutes);
	timeArr[2] = BCD2Dec(newTime.hours);
	timeArr[3] = BCD2Dec(newTime.dayOfmonth);
	timeArr[4] = BCD2Dec(newTime.month);
	uint8_t topYear = BCD2Dec(newTime.year>>8);
	uint8_t botYear = BCD2Dec(newTime.year&0x00FF);
	year[0] = topYear*100+botYear;
}

void getTimefromRTCtoDecCalendar(RTC_C_Calendar *time)
{
	newTime = RTC_C_getCalendarTime_mine();
	time->seconds =BCD2Dec(newTime.seconds);
	time->minutes = BCD2Dec(newTime.minutes);
	time->hours =BCD2Dec(newTime.hours);
	time->dayOfmonth = BCD2Dec(newTime.dayOfmonth);
	time->year = (BCD2Dec(newTime.year>>8))*100+BCD2Dec(newTime.year&0x00FF);
}



