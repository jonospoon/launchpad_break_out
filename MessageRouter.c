/*
 * MessageRouter.c
 *
 *  Created on: May 28, 2017
 *      Author: Jonathan
 */
#include "driverlib.h"
#include "MeshMessage.h"
#include "MessageRouter.h"
#include "timer.h"
#include "my_SystickTimer.h"
#include "my_NodeSettings.h"
#include "RFM96W.h"
#include "my_uart.h"

/// The last end-to-end sequence number to be used
/// Defaults to 0
uint8_t _lastE2ESequenceNumber;

/// The maximum number of hops permitted in routed messages.
/// If a routed message would exceed this number of hops it is dropped and ignored.
uint8_t              _max_hops;


/// Temporary mesage buffer
static RoutedMessage _tmpMessage;

/// Local routing table
RoutingTableEntry    _routes[RH_ROUTING_TABLE_SIZE];

bool setupRouter(uint8_t thisAddress)
{
	_max_hops = RH_DEFAULT_MAX_HOPS;
	_lastE2ESequenceNumber = 0;
	clearRoutingTable();
	return setupDatagram(thisAddress);
}

void setMaxHops(uint8_t max_hops)
{
	_max_hops = max_hops;
}

uint8_t maxHops(void)
{
	return _max_hops;
}

void addRouteFailure(uint8_t dest)
{
	RoutingTableEntry* route = getRouteTo(dest);
	route->RouteFailures++;
}
void subtractRouteFailure(uint8_t dest)
{
	RoutingTableEntry* route = getRouteTo(dest);
	if (route->RouteFailures>0)
		route->RouteFailures--;
}

uint8_t getRouteFailure(uint8_t dest)
{
	RoutingTableEntry* route = getRouteTo(dest);
	return route->RouteFailures;
}

void addRouteTo(uint8_t dest, uint8_t next_hop, bool state, uint8_t hops, bool server)
{
	uint8_t i;
	//If the destination is 0 then its not allowed to be added to the list. 0 is used only for
	//finding a server
	if (dest == 0)
		return;
	// First look for an existing entry we can update
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
	{
		if (_routes[i].dest == dest)
		{
			//Check the hops for adding over here
			if ((hops)>_routes[i].hops)
				return;

			_routes[i].dest = dest;
			_routes[i].next_hop = next_hop;
			_routes[i].state = state;
			_routes[i].hops = hops;
			_routes[i].server = server;
			_routes[i].RouteFailures = 0;
			sortRoutingTable();
			return;
		}
	}

	// Look for an invalid entry we can use
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
	{
		if (_routes[i].state == Invalid)
		{
			_routes[i].dest = dest;
			_routes[i].next_hop = next_hop;
			_routes[i].state = state;
			_routes[i].hops = hops;
			_routes[i].server = server;
			_routes[i].RouteFailures = 0;
			sortRoutingTable();
			return;
		}
	}

	// Need to make room for a new one
	retireOldestRoute();
	// Should be an invalid slot now
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
	{
		if (_routes[i].state == Invalid)
		{
			_routes[i].dest = dest;
			_routes[i].next_hop = next_hop;
			_routes[i].state = state;
			_routes[i].hops = hops;
			_routes[i].server = server;
			_routes[i].RouteFailures = 0;
			sortRoutingTable();
			return;
		}
	}

}

void sortRoutingTable(void)
{
	uint8_t i=0,j;
	bool swapped = true;
	while(swapped)
	{
		swapped = false;
		i++;
		for (j = 0; j < RH_ROUTING_TABLE_SIZE-i; j++)
		{
			if (_routes[j].hops>_routes[j+1].hops)
			{
				if(_routes[j].state&&_routes[j+1].state)
				{
					RoutingTableEntry tempSwap = _routes[j];
					_routes[j] = _routes[j+1];
					_routes[j+1] = tempSwap;
					swapped = true;
				}
			}
		}
	}
}

RoutingTableEntry* getRouteTo(uint8_t dest)
{
	uint8_t i;
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
		if (_routes[i].dest == dest && _routes[i].state != Invalid)
			return &_routes[i];
	return NULL;
}

RoutingTableEntry* getRouteToServer(void)
{
	uint8_t i;
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
		if (_routes[i].server == SERVER && _routes[i].state != Invalid)
			return &_routes[i];
	return NULL;
}

bool disableRouteIncluding(uint8_t dest)
{
	uint8_t i;
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
		if (_routes[i].dest==dest||_routes[i].next_hop==dest)
		{
			_routes[i].state=Invalid;
			//return true;
		}
	return false;
}
bool enableRouteIncluding(uint8_t dest)
{
	uint8_t i;
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
		if (_routes[i].dest==dest||_routes[i].next_hop==dest)
		{
			_routes[i].state=Valid;
			//return true;
		}
	return false;
}

bool deleteRouteTo(uint8_t dest)
{
	uint8_t i;
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
	{
		if (_routes[i].dest == dest)
		{
			deleteRoute(i);
			return true;
		}
	}
	return false;
}



void retireOldestRoute()
{
	// We start from the route with the most hops and only delete it if its not a server.
	uint8_t i;
	for (i = RH_ROUTING_TABLE_SIZE-1; i>=0;i--)
	{
		if (!_routes[i].server)
		{
			deleteRoute(i);
			return;
		}
	}
}

void deleteRoute(uint8_t index)
{
	// Delete a route by copying following routes on top of it
	memcpy(&_routes[index], &_routes[index+1],
			sizeof(RoutingTableEntry) * (RH_ROUTING_TABLE_SIZE - index - 1));
	_routes[RH_ROUTING_TABLE_SIZE - 1].state = Invalid;
}

void clearRoutingTable()
{
	uint8_t i;
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
		_routes[i].state = Invalid;
}


uint8_t UpdateRoutingTableInMemory(void)
{
	return updateRoutingTable(_routes);
}

uint8_t UpdateRoutingTableFromMemory(void)
{
	ReturnFullRoutingTable(_routes);
}

uint8_t sendtoFromSourceWait(uint8_t* buf, uint8_t len, uint8_t dest, uint8_t source, uint8_t flags)
{
	if (((uint16_t)len + sizeof(RoutedMessageHeader)) > RH_RF95_MAX_PAYLOAD_LEN)
		return RH_ROUTER_ERROR_INVALID_LENGTH;

	// Construct a RH RouterMessage message
	_tmpMessage.header.source = source;
	_tmpMessage.header.dest = dest;
	_tmpMessage.header.hops = 0;
	_tmpMessage.header.id = _lastE2ESequenceNumber++;
	_tmpMessage.header.flags = flags;
	memcpy(_tmpMessage.data, buf, len);

	return Meshroute(&_tmpMessage, sizeof(RoutedMessageHeader)+len);
}

uint8_t route(RoutedMessage* message, uint8_t messageLen)
{
	// Reliably deliver it if possible. See if we have a route:
	uint8_t next_hop = RH_BROADCAST_ADDRESS;

	if (message->header.dest != RH_BROADCAST_ADDRESS)
	{
		RoutingTableEntry* route = getRouteTo(message->header.dest);
		if (!route)
			return RH_ROUTER_ERROR_NO_ROUTE;
		next_hop = route->next_hop;
	}

	if (!sendWithTalkCheck((uint8_t*)message, messageLen, next_hop))
		return RH_ROUTER_ERROR_UNABLE_TO_DELIVER;


	return RH_ROUTER_ERROR_NONE;
}

uint8_t RouterrecvfromAck(uint8_t* buf, uint8_t* len, uint8_t* source, uint8_t* dest, uint8_t* id, uint8_t* flags, uint8_t from)
{
	uint8_t tmpMessageLen = sizeof(_tmpMessage);
	//uint8_t _from;
	uint8_t _to;
	uint8_t _id;
	uint8_t _flags;
	if (recvfromAck((uint8_t*)&_tmpMessage, &tmpMessageLen, &from, &_to, &_id, &_flags))
	{
		// Here we simulate networks with limited visibility between nodes
		// so we can test routing
		/*	if (_from == CLIENT_ADDRESS2 &&_to == CLIENT_ADDRESS3)
		{
			setAddress(CLIENT_ADDRESS3);
		}
		if (_from == CLIENT_ADDRESS2 &&_to == CLIENT_ADDRESS1)
		{
			setAddress(CLIENT_ADDRESS1);
		}*/

		/*^^^^^^^^^
		 * if (isFromaTag())
		 * {
		 * 	if (IamSinkNode())
		 * 	{
		 * 		wrap in mesh;
		 * 		return true;
		 * 	}
		 * 	RHMesh:sendtoWait(buf, len, dest=SinkNode, source=thisAddress(), flags);
		 * 	return false;
		 * }
		 ^^^^^^^^^*/
		if (isFromATag(_flags))
		{
			incrememntMemoryReceivedFromRhino();
			//send_UART("From a Rhino, flags: ");
			//send_uart_integer(_flags);
			if (AmIServer())
			{
				//send_UART("\nIm a server\n");
				if (source) *source  = from;
				if (dest)   *dest    = thisAddress();
				if (id)     *id      = _id;
				if (flags)  *flags   = _flags;
				uint8_t msgLen = tmpMessageLen - sizeof(RoutedMessageHeader);
				if (*len > msgLen)
					*len = msgLen;
				buf[1]= _tmpMessage.header.dest;
				buf[2]= _tmpMessage.header.source;
				buf[3]= _tmpMessage.header.hops;
				buf[4]= _tmpMessage.header.id;
				buf[5]= _tmpMessage.header.flags;
				memcpy(&buf[6], _tmpMessage.data, *len);
				*len+=6;
				return RouterRecvMessageForMe; // Its for you!
			}
			if (_flags != FLAGS_FROM_RHINO_TROUGH_MESH)
			{
				MeshFromTagtoServer((uint8_t*)&_tmpMessage,tmpMessageLen,FLAGS_FROM_RHINO_TROUGH_MESH);
				return RouterRecvMessagePassedOn;
			}
		}

		peekAtMessage(&_tmpMessage, tmpMessageLen);
		// See if its for us or has to be routed
		if (_tmpMessage.header.dest == thisAddress() || _tmpMessage.header.dest == RH_BROADCAST_ADDRESS)
		{
			// Deliver it here
			if (source) *source  = _tmpMessage.header.source;
			if (dest)   *dest    = _tmpMessage.header.dest;
			if (id)     *id      = _tmpMessage.header.id;
			if (flags)  *flags   = _tmpMessage.header.flags;
			uint8_t msgLen = tmpMessageLen - sizeof(RoutedMessageHeader);
			if (*len > msgLen)
				*len = msgLen;
			memcpy(buf, _tmpMessage.data, *len);
			return RouterRecvMessageForMe; // Its for you!
		}
		else if (   _tmpMessage.header.dest != RH_BROADCAST_ADDRESS
				&& _tmpMessage.header.hops++ < _max_hops)
		{
			// Maybe it has to be routed to the next hop
			// REVISIT: if it fails due to no route or unable to deliver to the next hop,
			// tell the originator. BUT HOW?
			//send_UART("Received from: ");
			//send_uart_integer(_tmpMessage.header.source);
			//send_UART("\nOn the way to:");
			//send_uart_integer(_tmpMessage.header.dest);
			if (Meshroute(&_tmpMessage, tmpMessageLen)== RH_ROUTER_ERROR_NONE)
				return RouterRecvMessagePassedOn;
		}
		// Discard it and maybe wait for another
	}
	return RouterRecvError;
}

void printRoutingTable()
{

	uint8_t i;
	for (i = 0; i < RH_ROUTING_TABLE_SIZE; i++)
	{
		send_uart_integer(i);
		send_UART(" Dest: ");
		send_uart_integer(_routes[i].dest);
		send_UART(" Next Hop: ");
		send_uart_integer(_routes[i].next_hop);
		send_UART(" State: ");
		send_uart_integer(_routes[i].state);
		send_UART(" Hops: ");
		send_uart_integer(_routes[i].hops);
		send_UART(" Server: ");
		send_uart_integer(_routes[i].server);
		send_UART("\n");

	}

}

void printServer(RoutingTableEntry* route)
{
	send_UART(" Dest: ");
	send_uart_integer(route->dest);
	send_UART(" Next Hop: ");
	send_uart_integer(route->next_hop);
	send_UART(" Hops: ");
	send_uart_integer_nextLine(route->hops);
}

RoutingTableEntry* getRouteBasedOnNumber(uint8_t position)
{
	if (_routes[position].state != Invalid)
		return &_routes[position];
	return NULL;
}

bool recvfromAckTimeoutRoute(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* source, uint8_t* dest, uint8_t* id, uint8_t* flags)
{
	uint8_t from;
	if (!recvCheckCanItalk(buf,timeout, &from))
		return false;
	runsystickFunction_ms(timeout);
	while (!SystimerReadyCheck())
	{
		if (waitAvailableTimeout(delayLeft()))
		{
			if (RouterrecvfromAck(buf, len, source, dest, id, flags, from)==RouterRecvMessageForMe )
				return true;
		}
		__no_operation();
	}
	return false;
}
