/*
 * my_gps.c
 *
 *  Created on: May 18, 2017
 *      Author: Jonathan
 */

#include "driverlib.h"
#include "my_gps.h"
#include "timer.h"
#include "my_externalFlags.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "my_RTC.h"
const eUSCI_UART_Config uartConfigGPS =
{
		EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
		78,                                     // BRDIV = 78
		2,                                       // UCxBRF = 2
		0,                                       // UCxBRS = 0
		EUSCI_A_UART_NO_PARITY,                  // No Parity
		EUSCI_A_UART_LSB_FIRST,                  // LSB First
		EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
		EUSCI_A_UART_MODE,                       // UART mode
		EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
};


#define NMEA_PREFIX_LENGTH	5
#define NMEA_MAX_LENGTH 82
char UartRX[NMEA_MAX_LENGTH];

GPS_data new_fix;
bool newGPS_reading = false;
bool updateTime = false;
uint8_t counter_reads=0; //UART receive buffer counter

void EUSCIA1_IRQHandler(void)
{
	uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A1_BASE);

	MAP_UART_clearInterruptFlag(EUSCI_A1_BASE, status);

	if(status & EUSCI_A_UART_RECEIVE_INTERRUPT && newGPS_reading==false)
	{
		UartRX[counter_reads] = MAP_UART_receiveData(EUSCI_A1_BASE);
		//MAP_UART_transmitData(EUSCI_A0_BASE, UartRX[counter_read]);
		//MAP_UART_transmitData(EUSCI_A0_BASE,counter_read);
		counter_reads++;
	}
	if ((UartRX[counter_reads-1]==0x0A) && (UartRX[counter_reads-2]==0x0D))
	{
		if (UartRX[3] =='R'|| UartRX[4]== 'M'|| UartRX[5]=='C')
		{
			newGPS_reading= true;

		}
		counter_reads = 0;
	}
	if (counter_reads>NMEA_MAX_LENGTH)
		counter_reads=0;
}

bool wait_for_NEWreading(uint8_t tries)
{
	uint8_t i;
	for(i=0; i < tries; i++)
	{
		if (newGPS_reading)
			return true;
		interrupt_delayms(100);
	}
	return false;

}


void disable_GPS_interrupt(void)
{
	MAP_Interrupt_disableInterrupt(INT_EUSCIA1);

}
void enable_GPS_interrupt(void)
{
	MAP_UART_enableInterrupt(INT_EUSCIA1,EUSCI_A_UART_RECEIVE_INTERRUPT);

}

void startup_GPS(void)
{
	MAP_GPIO_setOutputHighOnPin(GPIO_PORT_P3, GPIO_PIN0);
}

void shutdown_GPS(void)
{
	MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0);
}

void setup_GPS_uart(void)
{

	MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0);
	MAP_GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0);
	/* Selecting P2.2 and P2.3 in UART mode */
	GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P2,
			GPIO_PIN2 | GPIO_PIN3 , GPIO_PRIMARY_MODULE_FUNCTION);

	/* Configuring UART Module */
	MAP_UART_initModule(EUSCI_A1_BASE, &uartConfigGPS);
	/* Enable UART module */
	MAP_UART_enableModule(EUSCI_A1_BASE);
	/* Enabling interrupts */
	MAP_UART_enableInterrupt(EUSCI_A1_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
	MAP_Interrupt_enableInterrupt(INT_EUSCIA1);


}
/*bool nmea_parse(char *sentence, uint8_t length )
	{
		if(!nmea_valid(sentence,length))
		{
			return 0;
		}
			uint8_t comma_positions[14];
			count_commas(sentence, length, comma_positions);

			uint8_t hour = ((sentence[comma_positions[0]+1]-'0')*10)+(sentence[comma_positions[0]+2]-'0');
			uint8_t minute = ((sentence[comma_positions[0]+3]-'0')*10)+(sentence[comma_positions[0]+4]-'0');
			uint8_t second = ((sentence[comma_positions[0]+5]-'0')*10)+(sentence[comma_positions[0]+6]-'0');
			if (update_time)
			{

			}
			if (!(comma_positions[3]-comma_positions[2]==1))
			{
				char new_latitude[12];
				char new_longitude[12];

				uint8_t sattellites = ((sentence[comma_positions[6]+1]-'0')*10)+(sentence[comma_positions[6]+2]-'0');

				strncpy(new_latitude, &sentence[comma_positions[1]+1],comma_positions[3]-comma_positions[1]-1);
				strncpy(new_longitude, &sentence[comma_positions[3]+1],comma_positions[5]-comma_positions[3]-1);
			}
			MAP_UART_transmitData(EUSCI_A0_BASE,hour);
			MAP_UART_transmitData(EUSCI_A0_BASE,minute);
			MAP_UART_transmitData(EUSCI_A0_BASE,second);
			MAP_UART_transmitData(EUSCI_A0_BASE,'\n');

			return 1;
	}*/
bool nmea_parse(char *sentence, uint8_t length )
{
	sentence = UartRX;
	length = strlen(UartRX);
	if(!nmea_valid(sentence,length))
	{
		return 0;
	}
	uint8_t comma_positions[14];
	count_commas(sentence, length, comma_positions);

	new_fix.hour = ((sentence[comma_positions[0]+1]-'0')*10)+(sentence[comma_positions[0]+2]-'0');
	new_fix.minute = ((sentence[comma_positions[0]+3]-'0')*10)+(sentence[comma_positions[0]+4]-'0');
	new_fix.second = ((sentence[comma_positions[0]+5]-'0')*10)+(sentence[comma_positions[0]+6]-'0');
	strncpy(new_fix.date,&sentence[comma_positions[8]+1],comma_positions[9]-comma_positions[8]-1 );
	if (updateTime)
	{
		RTC_C_Calendar oldTime;
		getTimefromRTCtoDecCalendar(&oldTime);
		if (new_fix.hour!=oldTime.hours||new_fix.minute!=oldTime.minutes||(abs(new_fix.second-oldTime.seconds))>2)
		{
			oldTime.hours= new_fix.hour;
			oldTime.minutes = new_fix.minute;
			oldTime.seconds = new_fix.second;
			oldTime.dayOfmonth= (new_fix.date[0]-'0')*10+(new_fix.date[1]-'0');
			oldTime.month = (new_fix.date[2]-'0')*10+(new_fix.date[3]-'0');
			oldTime.year = (20*100)+(new_fix.date[4]-'0')*10+(new_fix.date[5]-'0');
			setTimefromDecCalander(&oldTime);
		}
		updateTime = false;
	}

	if (!(comma_positions[4]-comma_positions[3]==1))
	{
		/*char new_latitude[12];
				char new_longitude[12];
		 */
		//new_fix.sattellites = ((sentence[comma_positions[6]+1]-'0')*10)+(sentence[comma_positions[6]+2]-'0');

		strncpy(new_fix.new_latitude, &sentence[comma_positions[1]+1],comma_positions[3]-comma_positions[1]-1);
		strncpy(new_fix.new_longitude, &sentence[comma_positions[3]+1],comma_positions[5]-comma_positions[3]-1);
	}
	//MAP_UART_transmitData(EUSCI_A0_BASE,new_fix.hour);
	//MAP_UART_transmitData(EUSCI_A0_BASE,new_fix.minute);
	//MAP_UART_transmitData(EUSCI_A0_BASE,new_fix.second);
	//MAP_UART_transmitData(EUSCI_A0_BASE,'\n');
	newGPS_reading=false;
	return 1;
}
bool nmea_parse_internal(void)
{
	uint8_t length =strlen(UartRX);
	if(!nmea_valid(UartRX,length))
	{
		return 0;
	}
	uint8_t comma_positions[14];
	count_commas(UartRX, length, comma_positions);

	new_fix.hour = ((UartRX[comma_positions[0]+1]-'0')*10)+(UartRX[comma_positions[0]+2]-'0');
	new_fix.minute = ((UartRX[comma_positions[0]+3]-'0')*10)+(UartRX[comma_positions[0]+4]-'0');
	new_fix.second = ((UartRX[comma_positions[0]+5]-'0')*10)+(UartRX[comma_positions[0]+6]-'0');
	strncpy(new_fix.date,&UartRX[comma_positions[8]+1],comma_positions[9]-comma_positions[8]-1 );
	if (updateTime)
	{
		RTC_C_Calendar oldTime;
		getTimefromRTCtoDecCalendar(&oldTime);
		if (new_fix.hour!=oldTime.hours||new_fix.minute!=oldTime.minutes||(abs(new_fix.second-oldTime.seconds))>2)
		{
			oldTime.hours= new_fix.hour;
			oldTime.minutes = new_fix.minute;
			oldTime.seconds = new_fix.second;
			oldTime.dayOfmonth= (new_fix.date[0]-'0')*10+(new_fix.date[1]-'0');
			oldTime.month = (new_fix.date[2]-'0')*10+(new_fix.date[3]-'0');
			oldTime.year = (20*100)+(new_fix.date[4]-'0')*10+(new_fix.date[5]-'0');

			setTimefromDecCalander(&oldTime);
		}
		updateTime = false;
		interrupt_delayms(1000);
	}

	if (!(comma_positions[4]-comma_positions[3]==1))
	{
		/*char new_latitude[12];
				char new_longitude[12];
		 */
		//new_fix.sattellites = ((UartRX[comma_positions[6]+1]-'0')*10)+(UartRX[comma_positions[6]+2]-'0');

		strncpy(new_fix.new_latitude, &UartRX[comma_positions[1]+1],comma_positions[3]-comma_positions[1]-1);
		strncpy(new_fix.new_longitude, &UartRX[comma_positions[3]+1],comma_positions[5]-comma_positions[3]-1);
	}
	//MAP_UART_transmitData(EUSCI_A0_BASE,new_fix.hour);
	//MAP_UART_transmitData(EUSCI_A0_BASE,new_fix.minute);
	//MAP_UART_transmitData(EUSCI_A0_BASE,new_fix.second);
	//MAP_UART_transmitData(EUSCI_A0_BASE,'\n');
	newGPS_reading=false;
	return 1;

}

bool count_commas(char *sentence,uint8_t length,uint8_t *comma_positions )
{
	uint8_t count = 0;
	while (length>count)
	{
		if (sentence[count]==',')
		{
			*comma_positions++=count;
		}
		count++;
	}
	return true;
}
/*
 * nmea_valid() checks whether a valid NMEA GPGGA string has been sent to be parsed.
 * If the string is valid, then a true is returned
 * else it returns false
 */
bool nmea_valid(char *sentence, uint8_t length)
{
	if (length <39)
		return false;
	if (length>NMEA_MAX_LENGTH)
		return false;
	if (UartRX[0] != '$')
		return false;
	if (UartRX[3] !='R'|| UartRX[4]!= 'M'|| UartRX[5]!='C')
		return false;
	return true;
}



