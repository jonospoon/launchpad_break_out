/*
 * my_NodeSettings.c
 *
 *  Created on: Jun 21, 2017
 *      Author: Jonathan
 */


#include "my_NodeSettings.h"

bool AmIAserver = false;

uint8_t Mode = NormalMode;

uint32_t tempNumber;

void setNormalMode(){
	Mode = NormalMode;
	send_UART("NormalMode");
}

void settestMode()
{
	Mode = TestMode;
	send_UART("TestMode");
}


uint8_t returnMode()
{
	return Mode;

}


void set_AmIServer(bool serverStatus)
{
	//AmIAserver = serverStatus;
	updateServerStatus(serverStatus);
}

bool AmIServer(void)
{
	return FlashReadServer();
}

void incrememntNumberRestarts()
{
	updateRestarts(ReadRestarts() +1);
}
void incrementSDLogFiles()
{
    updateSDCardLogs(ReadSDLogs() +1);
}

void incrememntMemoryUpdates()
{
	tempNumber = 0;
	tempNumber = ReadMemoryUpdates();
	//send_UART("MemoryUpdate: ");
	//send_uart_integer_nextLine(++tempNumber);
	updateMemoryUpdates(++tempNumber);
}
void incrememntMemorySent()
{
	tempNumber = 0;
	tempNumber = ReadSent();
	send_UART("Sent: ");
	send_uart_integer_nextLine(++tempNumber);
	updateSent(tempNumber);
}
void incrememntMemoryRetries()
{
	tempNumber = 0;
	tempNumber = ReadRetries();
	send_UART("Retries: ");
	send_uart_integer_nextLine(++tempNumber);
	updateRetries(tempNumber);
}
void incrememntMemoryFails()
{
	tempNumber = 0;
	tempNumber = ReadFails();
	send_UART("Fails: ");
	send_uart_integer_nextLine(++tempNumber);
	updateFails(tempNumber);
}
void incrememntMemoryAttention()
{
	tempNumber = 0;
	tempNumber = ReadAttention();
	send_UART("Attention: ");
	send_uart_integer_nextLine(++tempNumber);
	updateAttention(tempNumber);
}
void incrememntMemoryReceived()
{
	tempNumber = 0;
	tempNumber = ReadRecieved();
	send_UART("Received: ");
	send_uart_integer_nextLine(++tempNumber);
	updateReceived(tempNumber);
}

void incrememntMemoryReceivedFromRhino()
{
	tempNumber = 0;
	tempNumber = ReadRecievedFromRhino();
	send_UART("Received From Rhino: ");
	send_uart_integer_nextLine(++tempNumber);
	updateReceivedFromRhino(tempNumber);
}
void incrememntMemoryTimeout()
{
	tempNumber = 0;
	tempNumber = ReadTimeout();
	send_UART("Timeout: ");
	send_uart_integer_nextLine(++tempNumber);
	updateTimeout(tempNumber);
}
void incrememntMemoryPassedOn()
{
	tempNumber = 0;
	tempNumber = ReadPassedOn();
	send_UART("PassedOn: ");
	send_uart_integer_nextLine(++tempNumber);
	updatePassedOn(tempNumber);
}
void incrememntMemoryReACk()
{
	tempNumber = 0;
	tempNumber = ReadReAck();
	send_UART("ReACK: ");
	send_uart_integer_nextLine(++tempNumber);
	updateReAck(tempNumber);
}
void incrememntMemoryReRoute()
{
	tempNumber = 0;
	tempNumber = ReadReRoute();
	send_UART("ReRoute: ");
	send_uart_integer_nextLine(++tempNumber);
	updateReRoute(tempNumber);
}
void incrememntMemoryReRouteRecevied()
{
	tempNumber = 0;
	tempNumber = ReadReRoute_Received();
	send_UART("ReRoute Received: ");
	send_uart_integer_nextLine(++tempNumber);
	updateReRouteReceived(tempNumber);
}
void incrememntMemory37Down()
{
	tempNumber = 0;
		tempNumber = Read37DOwn();
		send_UART("3.7V ShutDown: ");
		send_uart_integer_nextLine(++tempNumber);
		update37Down(tempNumber);
}

void incrememntMemory35Down()
{
	tempNumber = 0;
		tempNumber = ReadMem35Down();
		send_UART("3.5V ShutDown: ");
		send_uart_integer_nextLine(++tempNumber);
		updateMem35Down(tempNumber);
}




