/*
 * ms5607.c
 *
 *  Created on: Apr 11, 2017
 *      Author: Jonathan
 */

#include "timer.h"
#include "driverlib.h"
#include <stdint.h>
#include "spiDriver.h"
#include "ms5607.h"
#include "bithacks.h"

typedef struct
{
	uint16_t Reserved;
	uint16_t C1;  // Pressure sensitivity  SENS
	uint16_t C2;  // Pressure offset  OFF
	uint16_t C3;  // Temperature coefficient of pressure sensitivity  TCS
	uint16_t C4;  // Temperature coefficient of pressure offset  TCO
	uint16_t C5;  // Reference temperature
	uint16_t C6;  // Temperature coefficient of the temperature
	uint16_t CRC;
	int32_t dT;   // Difference between actual and reference temperature
}MS5607_t;

MS5607_t MS5607;
//CS = P3.6
/* set n-th bit in x */
//#define B_SET(x, n)      ((x) |= (1<<(n)))

/* unset n-th bit in x */
//#define B_UNSET(x, n)    ((x) &= ~(1<<(n)))

#define 	MS5607_CS_OUT			P3OUT
#define 	MS5607_CS_BIT			6

#define MS5607_CS_ENABLE	B_UNSET(MS5607_CS_OUT, MS5607_CS_BIT)
#define MS5607_CS_DISABLE	B_SET(	MS5607_CS_OUT, MS5607_CS_BIT)


/*-----Not entirely sure what we are doing here...
void MS5607WaitEOC(void)
{
	while(!B_IS_SET(MS5607_EOC_IN, MS5607_EOC_PIN))	// PIN low: conversion not over
	{
#ifdef 	SENSOR_EOC_INTERRUPT
		__disable_interrupt();
		B_UNSET(MS5607_EOC_IFG, MS5607_EOC_PIN);
		B_SET(MS5607_EOC_IE, MS5607_EOC_PIN);
		__enable_interrupt();
		//PowerEnterLowPower();
		__disable_interrupt();
		B_UNSET(MS5607_EOC_IE, MS5607_EOC_PIN);
		__enable_interrupt();
#else
		__no_operation();
#endif // SENSOR_EOC_INTERRUPT
	}
}
*/
void MS5607Stop(void)
{
	//spi_Close();
	MS5607_CS_DISABLE;
}

void MS5607Reset(void)
{
	//char printf_buff[140];
	//char printf_len = 0;

	spi_Open();
	MS5607_CS_ENABLE;
	spi_read_write(0x1E);
	MS5607_CS_DISABLE;
	interrupt_delayms(100);
}

void MS5607Start(void)
{
	MS5607Reset();
	//char printf_buff[140];
	//char printf_len = 0;


	//B_UNSET(MS5607_EOC_DIR, MS5607_EOC_PIN);
//	B_UNSET(MS5607_EOC_SEL, MS5607_EOC_PIN);
	//B_UNSET(MS5607_EOC_SEL2, MS5607_EOC_PIN);
/*
#ifdef 	SENSOR_EOC_INTERRUPT
	B_UNSET( MS5607_EOC_IES, MS5607_EOC_PIN);		// rising edge
#endif // SENSOR_EOC_INTERRUPT
*/
	//
	//B_UNSET(MS5607_CS_SEL, 	MS5607_CS_BIT);
	//B_UNSET(MS5607_CS_SEL2,	MS5607_CS_BIT);
	//B_SET(MS5607_CS_DIR, 	MS5607_CS_BIT);
	MS5607_CS_DISABLE;

	//SPIStart();
	/*
	MS5607_CS_ENABLE;
	spi_read_write(RESET);
	MS5607_CS_DISABLE;
	delay_ms(10);
	*/

	MS5607_CS_ENABLE;
	MS5607.C1 = spi_readInt( READ_C1 );
	MS5607_CS_DISABLE;
	interrupt_delayms(1);

	MS5607_CS_ENABLE;
	MS5607.C2 = spi_readInt( READ_C2 );
	MS5607_CS_DISABLE;
	interrupt_delayms(1);

	MS5607_CS_ENABLE;
	MS5607.C3 = spi_readInt( READ_C3 );
	MS5607_CS_DISABLE;
	interrupt_delayms(1);

	MS5607_CS_ENABLE;
	MS5607.C4 = spi_readInt( READ_C4 );
	MS5607_CS_DISABLE;
	interrupt_delayms(1);

	MS5607_CS_ENABLE;
	MS5607.C5 = spi_readInt( READ_C5 );
	MS5607_CS_DISABLE;
	interrupt_delayms(1);

	MS5607_CS_ENABLE;
	MS5607.C6 = spi_readInt( READ_C6 );
	MS5607_CS_DISABLE;
	interrupt_delayms(1);

	MS5607_CS_ENABLE;
	MS5607.CRC = spi_readInt( READ_CRC );
	MS5607_CS_DISABLE;


/*
//-----------------------------------------The commands are here
	MS5607SensorStartTemperatureSampling();
	uint32_t temperature_un = MS5607ReadTemperatureWhenAvailable();

	MS5607StartPressureSampling();
	uint32_t pressure_un = MS5607ReadPressureWhenAvailable();

	int32_t temperature = MS5607CompensateTemperature(temperature_un);
	uint32_t pressure = MS5607CompensatePressure(pressure_un);
//-----------------------------------------The commands end here
*/
}


int32_t MS5607GetTemp(void)
{
	MS5607SensorStartTemperatureSampling();
	interrupt_delayms(20);
	uint32_t temperature_un = MS5607ReadTemperatureWhenAvailable();
	int32_t temperature = MS5607CompensateTemperature(temperature_un);
	return temperature;
}

uint32_t MS5607GetPressure(void)
{
	MS5607StartPressureSampling();
	interrupt_delayms(20);
	uint32_t pressure_un = MS5607ReadPressureWhenAvailable();
	uint32_t pressure = MS5607CompensatePressure(pressure_un);
	return pressure;
}

void MS5607SensorStartTemperatureSampling(void)
{
	MS5607_CS_DISABLE;
	interrupt_delayms(1);
	MS5607_CS_ENABLE;
	spi_read_write( SENSOR_TEMPERATURE_OSR );
}

void MS5607StartPressureSampling (void)
{
	MS5607_CS_DISABLE;
	interrupt_delayms(1);
	MS5607_CS_ENABLE;
	spi_read_write( SENSOR_PRESSURE_OSR );
}


uint32_t MS5607ReadTemperatureWhenAvailable(void)
{
	uint32_t D2;
	//MS5607WaitEOC();
	MS5607_CS_DISABLE;
	interrupt_delayms(1);
	MS5607_CS_ENABLE;
	D2 = SPIRead24(ADC_READ);
	MS5607_CS_DISABLE;
	return D2;
}

uint32_t MS5607ReadPressureWhenAvailable(void)
{
	uint32_t D1;
	//MS5607WaitEOC();
	MS5607_CS_DISABLE;
	interrupt_delayms(1);
	MS5607_CS_ENABLE;
	D1 = SPIRead24(ADC_READ);
	MS5607_CS_DISABLE;
	return D1;
}

int32_t MS5607CompensateTemperature(uint32_t D2)	 // Compensate temperature
{
	// Difference between actual and reference temperature
	MS5607.dT = (int32_t)D2 - ((int32_t)MS5607.C5 << 8);
	// Actual temperature (-40 unsigned long long 85�C with 0.01�C resolution)
	int32_t TEMP = 2000 + ((MS5607.dT * (int64_t)MS5607.C6) >> 23);
	return TEMP;
}

uint32_t MS5607CompensatePressure(uint32_t D1)	 // Compensate pressure
{
	// Offset at actual temperature
	int64_t OFF = ((int64_t)MS5607.C2 << 17) + (((int64_t)MS5607.C4 * MS5607.dT) >> 6);
	// Sensitivity at actual temperature
	int64_t SENS = ((int64_t)MS5607.C1 << 16) + (((int64_t)MS5607.C3 * MS5607.dT) >> 7);
	// Temperature compensated pressure (10 to 1200mbar with 0.01mbar resolution)
	uint32_t P = (((D1 * SENS) >> 21) - OFF) >> 15;
	return  P;
}
