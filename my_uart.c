/*
 * my_uart.c
 *
 *  Created on: Apr 12, 2017
 *      Author: Jonathan
 */
#include <stdint.h>
#include "my_uart.h"
#include "driverlib.h"
#include "my_NodeSettings.h"
#include <string.h>

uint8_t counter_read=0; //UART receive buffer counter
bool UartActivity = false;
char UartRX[SIZE_BUFFER]; //Uart receive buffer
/***new***/
const eUSCI_UART_Config uartConfig =
{
 EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
 19,                                     // BRDIV = 78
 8,                                       // UCxBRF = 2
 85,                                       // UCxBRS = 0
 EUSCI_A_UART_NO_PARITY,                  // No Parity
 EUSCI_A_UART_LSB_FIRST,                  // LSB First
 EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
 EUSCI_A_UART_MODE,                       // UART mode
 EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
};
/***new***/
void send_uart_integer(uint32_t integer)
{
    char buffer[10];
    sprintf(buffer,"_%x_",integer);
    send_UART(buffer);

}
void send_uart_integer_nextLine(uint32_t integer)
{
    char buffer[10];
    sprintf(buffer,"_%x_",integer);
    send_UART(buffer);
    send_UART("\n");

}

void send_UART(char *buffer)
{
    int count = 0;
    while(strlen(buffer)>count)
    {
        MAP_UART_transmitData(EUSCI_A0_BASE,buffer[count++]);
    }
    uint32_t status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P1);
    GPIO_clearInterruptFlag(GPIO_PORT_P1, status);
   // write_string_toSD(buffer);
}



void PCuart_setup(void)
{

    /* Selecting P1.2 and P1.3 in UART mode */
    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
                                                   GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);
    /* Configuring UART Module */
    MAP_UART_initModule(EUSCI_A0_BASE, &uartConfig);

    /* Enable UART module */
    MAP_UART_enableModule(EUSCI_A0_BASE);
    /* Enabling interrupts for UART*/
    MAP_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
    MAP_Interrupt_setPriority(INT_EUSCIA0,0x40);
    MAP_Interrupt_enableInterrupt(INT_EUSCIA0);

}

void resetUARTArray()
{
    memset(UartRX,0x00,SIZE_BUFFER);
    counter_read=0;
}


/* EUSCI A0 UART ISR - Echoes data back to PC host */
void EUSCIA0_IRQHandler(void)
{

    uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A0_BASE);

    MAP_UART_clearInterruptFlag(EUSCI_A0_BASE, status);

    if(status & EUSCI_A_UART_RECEIVE_INTERRUPT)
    {

        UartRX[counter_read] =  MAP_UART_receiveData(EUSCI_A0_BASE);
        //UartActivity = true;
        //MAP_UART_transmitData(EUSCI_A0_BASE,UartRX[counter_read]);
        counter_read++;
    }
    if (UartRX[counter_read-1]==0x0D&&UartRX[counter_read-2]==0x0A)
        UartActivity = true;
    //if(counter_read == 80)
    // 	counter_read=0;
    if(counter_read==80)
    {counter_read=0;}
}

bool returnUartActivity()
{
    return UartActivity;
}

void UartCommands()
{
    counter_read=0;
    if (returnUartActivity())
    {
        UartActivity=false;
        if (returnMode() == NormalMode){
            if (UartRX[0]==0xE0)
            {
                settestMode();
            }
        }
        else if (returnMode() ==TestMode)
        {
            switch(UartRX[0])
            {
            case 0x80:
                setNormalMode();
                break;
            case 0xE1:
                printAllStats();
                break;
            case 0xE2:
                printRoutingTable();
                break;
            case 0xE3:
                zeroNodeStats();
                printAllStats();
                break;
            case 0xE4:
                zeroRoutingTable();
                UpdateRoutingTableFromMemory();
                printRoutingTable();
                break;
            case 0xE5:
                send_UART("This Address is: ");
                send_uart_integer_nextLine(thisAddress());
                break;
            case 0xE6:
                FlashWriteAddress(UartRX[1]);
                send_UART("Updated Address to: ");
                send_uart_integer_nextLine(thisAddress());
                break;
            case 0xE7:
                printServerOrNot();
                break;
            case 0xE8:
                set_AmIServer(CheckUartBufferServer(UartRX[1]));
                printServerOrNot();
                break;
            case 0xE9:
                addRouteTo(UartRX[1],UartRX[2],Valid,UartRX[3],CheckUartBufferServer(UartRX[4]));
                printRoutingTable();
                UpdateRoutingTableInMemory();
                break;
            case 0xEA:
                zeroNetworkStats();
                printAllStats();
                break;
            case 0xEB:
            	printTemp();
				break;
            case 0xEC:
            	printTime();
            	break;
            case 0xE0:
            	send_UART("Currently in TestMode");
            	break;
            case 0xED:
            	checkForServer();
            	break;
            case 0xEE:
            	set_RTCfromGPS();
            	printTime();
            	break;
            case 0xEF:
            	send_UART("");
            	uint8_t timeArr [5];
            	timeArr[0] = UartRX[1];
            	timeArr[1] = UartRX[2];
            	timeArr[2] = UartRX[3];
            	timeArr[3] = UartRX[4];
            	timeArr[4] = UartRX[5];
            	send_uart_integer_nextLine(timeArr[3]);
            	uint8_t TopYear = UartRX[6];
            	uint8_t botYear = UartRX[7];
            	uint16_t Year = (TopYear*100)+(botYear);
            	setTimefromDecArray(timeArr,Year);
            	printTime();
            	break;

            default:
                send_UART("Unknown Command");

            }
            resetUARTArray();
        }

    }
}

void printTemp()
{
	send_UART("Temperature: ");
		send_uart_integer_nextLine(MS5607GetTemp());

}

void printTime()
{
	char buffer [12];
	static uint8_t RTC_time [5];
	static uint16_t year;
	getTimefromRTCtoDecArray(RTC_time,&year);
	sprintf(buffer,"Time %d %d:%d %d:%d:%d\n",year,RTC_time[4],RTC_time[3], RTC_time[2],RTC_time[1] , RTC_time[0]);
	send_UART(buffer);
}

void printServerOrNot()
{
    if (AmIServer())
        send_UART("Im a server\n");
    else
        send_UART("Not a server\n");
}

bool CheckUartBufferServer(uint8_t buffer)
{
    if (buffer == 0xF0)
    {
        return SERVER;
    }
    else if (buffer== 0x0F)
    {
        return NOT_SERVER;
    }
    send_UART("Invalid Server Variable");
    return NOT_SERVER;
}

void checkUartActivity()
{
    if (returnUartActivity())
    {
        UartCommands();
    }
}
