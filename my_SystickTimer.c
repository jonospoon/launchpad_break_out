/*
 * my_SystickTimer.c
 *
 *  Created on: May 23, 2017
 *      Author: Jonathan
 */
#include "driverlib.h"
#include "my_SystickTimer.h"
/************** SYSTICK TIMER **************/
uint16_t delay=0;
bool timerReady=false;
/************** SYSTICK TIMER **************/

void SysTick_Handler(void) //INTERRUPT ROUTINE
{
   // MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
	if (delay==0)
		{
			timerReady = true;
			MAP_SysTick_disableInterrupt();
		}
	delay--;

}

void setupSysTick_ms(void)
{
	//Sets the sysTick to 1 millisecond
	MAP_SysTick_disableModule();
	MAP_SysTick_setPeriod(12000);
	MAP_SysTick_enableModule();
}
void setupSysTick_second(void)
{
	//Sets the sysTick to 1 second
	MAP_SysTick_disableModule();
	MAP_SysTick_setPeriod(12000000);
	MAP_SysTick_enableModule();
}


void runsystickFunction_ms(uint16_t ms_delay)
{
	setupSysTick_ms();
	delay= ms_delay;
	timerReady=false;
	MAP_SysTick_enableInterrupt();
}
void runSystickFunction_second(uint16_t second_delay)
{
	setupSysTick_second();
	delay= second_delay;
	timerReady=false;
	MAP_SysTick_enableInterrupt();
}

void stopSystick(void)
{
	delay=0;
	MAP_SysTick_disableInterrupt();
}

bool SystimerReadyCheck(void)
{
	return timerReady;
}

uint16_t delayLeft(void)
{
	return delay;
}
