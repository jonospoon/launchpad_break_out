/*
 * ReliableMessage.c
 *
 *  Created on: May 23, 2017
 *      Author: Jonathan
 */

#include "driverlib.h"
#include"ReliableMessage.h"

#include "stdlib.h"
#include "timer.h"


/// Count of retransmissions we have had to send
uint32_t _retransmissions;

/// The last sequence number to be used
/// Defaults to 0
uint8_t _lastSequenceNumber;

// Retransmit timeout (milliseconds)
/// Defaults to 800
uint32_t _timeout;

//Successful send of data
uint32_t _successSend;

//Sending Failures
uint32_t _sendFailure;

// Retries (0 means one try only)
/// Defaults to 3
uint8_t _retries;

uint16_t _Re_acks;

/// Array of the last seen sequence number indexed by node address that sent it
/// It is used for duplicate detection. Duplicated messages are re-acknowledged when received
/// (this is generally due to lost ACKs, causing the sender to retransmit, even though we have already
/// received that message)
uint8_t _seenIds[256];

bool setupDatagram(uint8_t thisAddress)
{
	_retransmissions = 0;
	_lastSequenceNumber = 0;
	_successSend=0;
	_sendFailure = 0;
	_Re_acks = 0;
	_timeout = RH_DEFAULT_TIMEOUT;
	_retries = RH_DEFAULT_RETRIES;
	return RFM96W_driver_init(thisAddress);
}



void setTimeout(uint16_t timeout)
{
	_timeout = timeout;
}

////////////////////////////////////////////////////////////////////
void setRetries(uint8_t retries)
{
	_retries = retries;
}

////////////////////////////////////////////////////////////////////
uint8_t retries()
{
	return _retries;
}

uint32_t returnSendFailures()
{
	return _sendFailure;
}

uint32_t returnSuccessSend()
{
	return _successSend;
}

uint32_t retransmissions()
{
	return _retransmissions;
}

void resetRetransmissions()
{
	_retransmissions = 0;
}

bool sendWithTalkCheck(uint8_t* buf, uint8_t len, uint8_t address)
{
	//The idea is that the node will first request from the other node if it is currently busy
	//The other Node will either reply with an Ack, meaning it is not busy, or it will not reply.
	//Once the ack is obtained from the receiving node the full message will be sent with an ACK expected.

	//Create an empty message to send for the talk check
	//REVISIT: Send the length (len) of the final message to be sent so that the timing of the receive node
	//can be dynamically set to wait for an exact time for the incoming message.
	uint8_t checkMessage = 'Z';

	//This will then go into the sending with checks for an Ack
	//REVISIT: change sendtoWait to require a timing amount for how long
	//should be waited for an Ack.


	//MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);/*#### TEST ####*/
	uint8_t Address_after_ack = address;
	send_UART("Attempting to get attention of node: ");
	send_uart_integer(Address_after_ack);
	send_UART("\n");
	if (sendtoWait(&checkMessage,sizeof(checkMessage), &Address_after_ack, FLAGS_CAN_I_TALK))
	{
		//MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);/*#### TEST ####*/
		send_UART("Got attention, sending full message to: ");
		send_uart_integer(Address_after_ack);
		send_UART("\n");
		//If the Check message was succesfully received we can then send the full message as the
		//receviver will know to wait for a set time for the full message
		if (sendtoWait(buf, len, &Address_after_ack, RH_FLAGS_NONE))
		{
			/*_successSend++;
			send_UART("Sent: ");
			send_uart_integer(_successSend);
			send_UART("\n");*/
			incrememntMemorySent();
			return true;
		}
	}
	/*_sendFailure++;
	send_UART("Fail: ");
	send_uart_integer(_sendFailure);
	send_UART("\n");*/
	incrememntMemoryFails();
	return false;
}


bool sendtoWait(uint8_t* buf, uint8_t len, uint8_t* address, uint8_t flags)
{
	// Assemble the message
	//thisSequenceNumber Could place this outside the loop in sendWithTalkCheck and only increment
	//when the full message is sent, however the ID is only used for acknowledgement and therefore
	//should be unique between transmissions so as to avoid a previous transmission been regarded as an ack
	uint8_t thisSequenceNumber = ++_lastSequenceNumber;
	uint8_t retries = 0;
	while (retries++ <= _retries) //Check the # of current retries is less than the set amount
	{

		setRFMHeaderID(thisSequenceNumber);
		setRFMHeaderFlags(RH_FLAGS_NONE, 0xFF); // Clear the ACK flag
		setRFMHeaderFlags(flags,RH_FLAGS_NONE);
		//MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0); /*#### TEST ####*/
		sendto(buf, len, *address); //RHDatagram (sets header to and then sends)
		waitPacketSent(); //Waits for the packet to finish sending
		MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0); /*#### TEST ####*/
		// Never wait for ACKS to broadcasts:
		if (flags!=FLAGS_CAN_I_TALK)
		{
			if (*address == RH_BROADCAST_ADDRESS)
				return true; //No ack is waited for if the message is broadcast
		}
		if (retries > 1)
		{
			/*_retransmissions++;
			send_UART("Retransmission ");
			send_uart_integer(_retransmissions);
			send_UART("\n");*/
			incrememntMemoryRetries();
		}
		//unsigned long thisSendTime = millis(); // Timeout does not include original transmit time

		// Compute a new timeout, random between _timeout and _timeout*2
		// This is to prevent collisions on every retransmit
		// if 2 nodes try to transmit at the same time

		//uint16_t timeout = _timeout + (_timeout * rand())/_timeout; //USE ME
		uint16_t timeout =  _timeout + (rand()%_timeout);
		runsystickFunction_ms(timeout);
		//send_UART(timeout);
		uint8_t from, to, id, flags;
		while (!SystimerReadyCheck())
		{
			if (waitAvailableTimeout(delayLeft()))
			{

				if (recvfrom(0, 0, &from, &to, &id, &flags)) // Discards the message
				{
					uint8_t flags =headerFlags();
					// Now have a message: is it our ACK?
					if (   ((from == *address)||(*address==RH_BROADCAST_ADDRESS))
							&& to == thisAddress()
							&& (flags & RH_FLAGS_ACK)
							&& (id == thisSequenceNumber))
					{
						//*****ONLY FOR RHINO*******
						//After a RTS is sent by a rhino tag, it must
						//only transmit to the node which sends a CTS.
						//Therefore it wont send the same msg to 2 nodes
						/*
						if (*address ==RH_BROADCAST_ADDRESS)
							*address = from;
						*/

						// Its the ACK we are waiting for
						//send_UART("Acknowledge received\n");
						return true;
					}
					else if (   !(flags & RH_FLAGS_ACK)
							&& (id == _seenIds[from]))
					{
						// This is a request we have already received. ACK it again
						acknowledge(id, from);
					}
					// Else discard it
				}
			}
			// Not the one we are waiting for, maybe keep waiting until timeout exhausted
			__no_operation();
		}
		// Timeout exhausted, maybe retry
		__no_operation();
	}
	// Retries exhausted
	return false;
}
void acknowledge(uint8_t id, uint8_t from)
{
	//send_UART("sending Acknowledge\n");
	setRFMHeaderID(id);
	setRFMHeaderFlags(RH_FLAGS_ACK,0x00);
	// We would prefer to send a zero length ACK,
	// So we send an ACK of 1 octet
	// REVISIT: should we send the RSSI for the information of the sender?
	// uint8_t header=myHeaderFlags();

	uint8_t ack = '!';
	sendto(&ack, sizeof(ack), from);
	waitPacketSent();
}

bool recvFromAck_CheckTalkFlag(uint8_t* buf, uint8_t* from)
{
	uint8_t _from;
	uint8_t _to;
	uint8_t _id;
	uint8_t _flags;
	uint16_t delay_to_Ack = 0;
	// Get the message before its clobbered by the ACK (shared rx and tx buffer in some drivers
	if (available() && recvfrom(buf, 0, &_from, &_to, &_id, &_flags))
	{
		//MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P2, GPIO_PIN0); /*#### TEST ####*/
		// Never ACK an ACK
		if (!(_flags & RH_FLAGS_ACK))
		{
			// Its a normal message for this node, not an ACK
			//Also check whether it is the can I talk message
			if (_flags & FLAGS_CAN_I_TALK) /*^^^^^^^^^ (_to == thisAddress()||isFromaTag()) ^^^^^^^^^ */
			{
				if (busy_doing_route_find ==true)
				{
						if (headerTo()!=thisAddress())
						return false;

				}
				_from = headerFrom();
				// Its not a broadcast, so ACK it
				// Acknowledge message with ACK set in flags and ID set to received ID
				if (_to == RH_BROADCAST_ADDRESS)
				{

					delay_to_Ack = abs(lastRssi());
					if (!AmIServer())
					{
						delay_to_Ack *= 5;
						delay_to_Ack += rand()%137;
					}
					interrupt_delayms(delay_to_Ack);

				}
				send_uart_integer(delay_to_Ack);
				//send_uart_integer(delay_to_Ack);
				delay_to_Ack = 0;
				//interrupt_delayms(20);
				acknowledge(_id, _from);
				*from = _from;
				//We now jump out to recvfromAckTimeout where it will check the flags and
				//if the FLAGS_CAN_I_TALK flag is set then it will return back to recvfromAck loop
				//to receive the next message
				return true;
			}
			else if (_id == _seenIds[_from] && _to == thisAddress())
			{
				acknowledge(_id, _from);
				/*_Re_acks++;
				send_UART("RE_ACK: ");
				send_uart_integer(_Re_acks);
				send_UART("\n");*/
				incrememntMemoryReACk();
			}
			// Else just re-ack it and wait for a new one
		}
	}
	// No message for us available
	return false;
}



bool recvfromAck(uint8_t* buf, uint8_t* len, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
	uint8_t _from;
	uint8_t _to;
	uint8_t _id;
	uint8_t _flags;
	// Get the message before its clobbered by the ACK (shared rx and tx buffer in some drivers
	if (available() && recvfrom(buf, len, &_from, &_to, &_id, &_flags))
	{
		//Check that the message received is from the node which requested to send through the
		//'Can i talk' method. The address from is stored after by the canITalk method and
		//is sent here to ensure that only messages from that node are received.
		if (_from != *from)
			return false;
		// Never ACK an ACK
		if (!(_flags & RH_FLAGS_ACK))
		{
			// Its a normal message for this node, not an ACK
			//Also check whether the msg is actually from a tag..
			if (_to == thisAddress()||isFromATag(_flags)) /*^^^^^^^^^ (_to == thisAddress()||isFromaTag()) ^^^^^^^^^ */
			{

				// Its not a broadcast, so ACK it
				// Acknowledge message with ACK set in flags and ID set to received ID
				acknowledge(_id, _from);
			}
			// If we have not seen this message before, then we are interested in it
			if (_id != _seenIds[_from])
			{
				if (from)  *from =  _from;
				if (to)    *to =    _to;
				if (id)    *id =    _id;
				if (flags) *flags = _flags;
				_seenIds[_from] = _id;
				//We now jump out to recvfromAckTimeout where it will check the flags and
				//if the FLAGS_CAN_I_TALK flag is set then it will return back to recvfromAck loop
				//to receive the next message
				return true;
			}
			// Else just re-ack it and wait for a new one
		}
	}
	// No message for us available
	return false;
}

bool isFromATag(uint8_t flags)
{
	return flags&FLAGS_FROM_A_RHINO;
}

bool recvCheckCanItalk(uint8_t* buf,uint16_t timeout, uint8_t* from)
{
	runsystickFunction_ms(timeout);
	while (!SystimerReadyCheck())
	{
		if (waitAvailableTimeout(delayLeft()))
		{
			if (recvFromAck_CheckTalkFlag(buf, from))
				return true;
		}
		__no_operation();
	}
	//setModeIdle();
	return false;
}

bool recvfromAckTimeout(uint8_t* buf, uint8_t* len, uint16_t timeout, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
	//unsigned long starttime = millis();
	//int32_t timeLeft;
	//We are first going to check that the CAN I TALK flag has been received
	if (!recvCheckCanItalk(buf,timeout, from))
		return false;
	//We have received a request to transmit
	//We now wait for the full message to be transmitted
	runsystickFunction_ms(timeout);
	while (!SystimerReadyCheck())
	{
		if (waitAvailableTimeout(delayLeft()))
		{
			//This is the first loop to check for the Can I talk flag
			if (recvfromAck(buf, len, from, to, id, flags))
				return true;
		}
		__no_operation();
	}

	return false;
}



/*********Setup for this node address**********/


bool RFM96W_driver_init(uint8_t thisAddress)
{
	srand(123);
	bool started = init_rfm96();
	if (started)
	{
		setAddress(thisAddress);
	}
	return started;
}


/*void setAddress(uint8_t thisAddress)
{
	setRFMAddress(thisAddress);
	// Use this address in the transmitted FROM header
	setRFMHeaderFrom(thisAddress);
	_thisAddress = thisAddress;
}*/

bool sendto(uint8_t* buf, uint8_t len, uint8_t address)
{
	setRFMHeaderTo(address);
	return send(buf, len);
}

bool recvfrom(uint8_t* buf, uint8_t* len, uint8_t* from, uint8_t* to, uint8_t* id, uint8_t* flags)
{
	if (recv(buf, len))
	{
		if (from)  *from =  headerFrom();
		if (to)    *to =    headerTo();
		if (id)    *id =    headerId();
		if (flags) *flags = headerFlags();
		return true;
	}
	return false;
}

