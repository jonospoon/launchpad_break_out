/*
 * my_gps.h
 *
 *  Created on: May 18, 2017
 *      Author: Jonathan
 */

#ifndef MY_GPS_H_
#define MY_GPS_H_

typedef struct{
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	char date[6];
	char new_latitude[12];
	char new_longitude[12];
}GPS_data;

bool nmea_parse(char *sentence, uint8_t length );
bool nmea_parse_internal(void);
void setup_GPS_uart(void);
void disable_GPS_interrupt(void);
void enable_GPS_interrupt(void);
bool count_commas(char *sentence,uint8_t length,uint8_t *comma_positions );
bool nmea_valid(char *sentence, uint8_t length);
/*
 * Used to wait for a new GPS reading. A user can enter the number of 100 milliseconds
 * they want to wait for the new reading. A GPS usually updates every second, so it
 * would be advisable to wait at least 1000 ms, therefore setting tries = 10.
 * Returns: true if a new reading is obtained. false if no reading is obtained
 */
bool wait_for_NEWreading(uint8_t tries);
/*
 * Sets pin3.0 to high, enabling the GPS module
 */
void startup_GPS(void);
/*
 * Sets pin3.0 to low, disabling the GPS module
 */
void shutdown_GPS(void);



#endif /* MY_GPS_H_ */
