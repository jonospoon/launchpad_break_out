/*
 * my_NodeSettings.h
 *
 *  Created on: Jun 21, 2017
 *      Author: Jonathan
 */

#ifndef MY_NODESETTINGS_H_
#define MY_NODESETTINGS_H_
#include "driverlib.h"
#include <stdio.h>
#include "stdlib.h"
#include <stdint.h>
#include "my_externalFlags.h"
#include "timer.h"
#include "ms5607.h"
#include "my_RTC.h"
#include "my_gps.h"
#include "my_uart.h"
#include "my_SystickTimer.h"
#include"ReliableMessage.h"
#include "MessageRouter.h"
#include "fatfs/src/diskio.h"
#include "fatfs/src/ff.h"
#include "MeshMessage.h"
#include "scheduler.h"
#include "my_gps.h"
#include "my_gsmModem.h"
#include "my_Message.h"
#include "my_powerSettings.h"
#include "my_Flash.h"

#define NormalMode 0
#define TestMode 1

extern bool busy_doing_route_find;

void setNormalMode();
void settestMode();
uint8_t returnMode();

//Set the status of this node as to whether it is a Server or not
void set_AmIServer(bool serverStatus);

//Rturns the AmIAServer boolean indiciating whether this node is a server or not
bool AmIServer(void);

void incrememntMemoryUpdates();
void incrememntNumberRestarts();
void incrememntMemorySent();
void incrememntMemoryRetries();
void incrememntMemoryFails();
void incrememntMemoryAttention();
void incrememntMemoryReceived();
void incrememntMemoryReceivedFromRhino();
void incrememntMemoryTimeout();
void incrememntMemoryPassedOn();
void incrememntMemoryReACk();
void incrememntMemoryReRoute();
void incrememntMemoryReRouteRecevied();
void incrementSDLogFiles();
void incrememntMemory37Down();
void incrememntMemoryReRouteRecevied();

#endif /* MY_NODESETTINGS_H_ */
